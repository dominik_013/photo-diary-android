package at.fhhagenberg.photo_diary.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import org.joda.time.DateTime;

import java.util.List;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.adapter.ActiveDiaryEntryAdapter;
import at.fhhagenberg.photo_diary.helper.DiarySettings;
import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.sql.DatabaseManager;

/**
 * A class that is handling notifications.
 */
public class NotificationService {

    /**
     * This method is creating / updating all notifications for all active diary-entries
     * where notifications are enabled
     *
     * @param context context
     */
    static synchronized void createOrUpdateNotificationsForActiveEntries(final Context context) {
        List<DiaryEntry> diaryEntryList = DatabaseManager.getInstance(context).getAllCurrentActiveDiaryEntriesWithNotifications();
        boolean areNotificationsGloballyEnabled = DiarySettings.isNotificationEnabled(context);
        Log.i("Notification", "Currently there are " + diaryEntryList.size() + " notifications pending");
        cancelNotificationsForEntries(context, diaryEntryList);
        for (DiaryEntry diaryEntry : diaryEntryList) {
            if (diaryEntry.isNotificationEnabled() && areNotificationsGloballyEnabled) {
                Bundle extrasForNotificationIntent = new Bundle();
                extrasForNotificationIntent.putLong(NotificationSchedulerIntentService.EXTRA_DIARY_ENTRY_ID, diaryEntry.getExternalId());
                extrasForNotificationIntent.putInt(ActiveDiaryEntryAdapter.EXTRA_SELECTED_DIARY_ENTRY, diaryEntry.getExternalId());

                scheduleNotification(
                        context,
                        NotificationSchedulerIntentService.ACTION_ISSUE_DIARY_ENTRY_NOTIFICATION,
                        diaryEntry.getExternalId(),
                        calculateNextNotificationDate(diaryEntry),
                        diaryEntry.getName(),
                        context.getResources().getString(R.string.notification_content),
                        extrasForNotificationIntent);
            }
        }
    }

    /**
     * This method is scheduling a notification for a specific diary-entry
     *
     * @param context                  context
     * @param intentServiceAction      The action which is going to be passed to the NotificationSchedulerIntentService
     * @param notificationId           The id for the notification, which is represented by the diary-entry-id
     * @param notificationDate         The date when the notification should fire
     * @param title                    The title the notification should contain
     * @param contentText              The message the notification should contain
     * @param notificationIntentExtras The intent which is passed to the activity, when the notification is clicked
     */
    private static void scheduleNotification(final Context context, String intentServiceAction, int notificationId,
                                             DateTime notificationDate, String title, String contentText, Bundle notificationIntentExtras) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent notificationServiceIntent = new Intent(intentServiceAction, null, context, NotificationSchedulerIntentService.class);

        notificationServiceIntent.putExtra(NotificationSchedulerIntentService.INTENT_EXTRA_NOTIFICATION_TITLE, title);
        notificationServiceIntent.putExtra(NotificationSchedulerIntentService.INTENT_EXTRA_NOTIFICATION_CONTENT_TEXT, contentText);
        notificationServiceIntent.putExtra(NotificationSchedulerIntentService.INTENT_EXTRA_NOTIFICATION_ID, notificationId);
        notificationServiceIntent.putExtras(notificationIntentExtras);

        Log.i("Notification", "Next notification for " + title + " is going to be scheduled at: " + notificationDate.toString());

        PendingIntent pendingIntent = PendingIntent.getService(context, notificationId, notificationServiceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP, notificationDate.getMillis(), pendingIntent);
    }

    /**
     * Cancels all notifications if we recreate the notifications for all diary-entries,
     * to prevent having double the notifications
     *
     * @param context        context
     * @param diaryEntryList The list of all diary-entries which will fire notifications
     */
    private static void cancelNotificationsForEntries(final Context context, List<DiaryEntry> diaryEntryList) {
        for (DiaryEntry diaryEntry : diaryEntryList) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent notificationIntent = new Intent(NotificationSchedulerIntentService.ACTION_ISSUE_DIARY_ENTRY_NOTIFICATION, null, context, NotificationSchedulerIntentService.class);
            PendingIntent pendingIntent = PendingIntent.getService(context, diaryEntry.getExternalId(), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.cancel(pendingIntent);
        }
    }

    /**
     * Removes notifications for a specific diary-entry
     *
     * @param context    context
     * @param diaryEntry The diary-entry for which we want to remove and cancel all notifications
     */
    static void removeNotificationsForDiaryEntry(final Context context, DiaryEntry diaryEntry) {
        diaryEntry.setNotificationEnabled(false);
        DatabaseManager.getInstance(context).createOrUpdateDiaryEntry(diaryEntry);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent notificationIntent = new Intent(NotificationSchedulerIntentService.ACTION_ISSUE_DIARY_ENTRY_NOTIFICATION, null, context, NotificationSchedulerIntentService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, diaryEntry.getExternalId(), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);

        Log.i("Notification", "Canceled notification for " + diaryEntry.getName());
    }

    /**
     * Calculates the DateTime for the next notification to be shown for a specific diary-entry
     *
     * @param diaryEntry The diary-entry for which we want to calculate the new notification date
     * @return The new notification date
     */
    private static DateTime calculateNextNotificationDate(DiaryEntry diaryEntry) {
        DateTime notificationDate = diaryEntry.getNextNotification() == null ? DateTime.now().withTime(9, 0, 0, 0) : diaryEntry.getNextNotification();

        if (notificationDate.isAfter(DateTime.now())) {
            return notificationDate;
        }

        switch (diaryEntry.getNotificationMode()) {
            case NotificationMode.THREE_TIMES_A_DAY:
                int hour = 8;
                if (DateTime.now().getHourOfDay() > 14)
                    hour = 14;
                else if (DateTime.now().getHourOfDay() > 20) {
                    hour = 20;
                }
                notificationDate = notificationDate.withHourOfDay(hour);
                notificationDate = notificationDate.plusHours(6);
                break;
            case NotificationMode.DAILY:
                notificationDate = notificationDate.plusDays(1);
                break;
            case NotificationMode.ALL_THREE_DAYS:
                notificationDate = notificationDate.plusDays(3);
                break;
            case NotificationMode.WEEKLY:
                notificationDate = notificationDate.plusDays(7);
                break;
        }

        return notificationDate;
    }

    /**
     * The NotificationMode which determines how often
     * notifications for specific diary-entries are sent to the user
     */
    public static class NotificationMode {
        /**
         * User doesn't receive any notifications
         */
        public static final int NONE = -1;

        /**
         * Notifications are sent three times a day
         * At 8:00 AM, at 2:00 PM and at 8:00 PM
         */
        public static final int THREE_TIMES_A_DAY = 0;

        /**
         * Notifications are sent daily at 9:00 AM
         */
        public static final int DAILY = 1;

        /**
         * Notifications are sent all three days at 9:00 AM
         */
        public static final int ALL_THREE_DAYS = 2;

        /**
         * Notifications are sent once every week at 9:00 AM
         */
        public static final int WEEKLY = 3;
    }
}