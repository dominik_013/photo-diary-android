package at.fhhagenberg.photo_diary.service;


import android.content.Context;

import at.fhhagenberg.photo_diary.model.DiaryEntry;

interface IDiaryEntryService {
    /**
     * Creates a diary-entry
     *
     * @param context    context
     * @param diaryEntry The diary-entry which should be created
     */
    void createDiaryEntry(final Context context, DiaryEntry diaryEntry);

    /**
     * Completes a diary-entry
     *
     * @param context    context
     * @param diaryEntry The diary-entry which should be updated
     */
    void completeDiaryEntry(final Context context, DiaryEntry diaryEntry);

    /**
     * Deletes a diary-entry
     *
     * @param context    context
     * @param diaryEntry The diary-entry which should be deleted
     */
    void deleteDiaryEntry(final Context context, DiaryEntry diaryEntry);
}
