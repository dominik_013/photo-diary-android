package at.fhhagenberg.photo_diary.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.ui.DiaryEntryDetailActivity;

/**
 * Service that fires notification which were created by the NotificationService
 *
 * @see at.fhhagenberg.photo_diary.service.NotificationService
 */
public class NotificationSchedulerIntentService extends IntentService {
    public static final String EXTRA_DIARY_ENTRY_ID = "diaryEntryId";
    public static final String ACTION_ISSUE_DIARY_ENTRY_NOTIFICATION = "actionIssueDiaryEntryNotification";
    public static final String INTENT_EXTRA_NOTIFICATION_CONTENT_TEXT = "notificationContentText";
    public static final String INTENT_EXTRA_NOTIFICATION_TITLE = "notificationTitle";
    public static final String INTENT_EXTRA_NOTIFICATION_ID = "notificationId";

    private static final int DEFAULT_NOTIFICATION_ID = 0;

    /**
     * Empty constructor is required for IntentService
     */
    public NotificationSchedulerIntentService() {
        super("NotificationSchedulerIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final String action = intent.getAction();

        switch (action) {
            case ACTION_ISSUE_DIARY_ENTRY_NOTIFICATION:
                handleActionIssueDiaryEntryNotification(intent);
                break;
        }
    }

    /**
     * Handle the action for diary-entry-notifications
     *
     * @param intent The intent which was received from the AlarmManager
     */
    private void handleActionIssueDiaryEntryNotification(Intent intent) {
        Intent notificationIntent = new Intent(this, DiaryEntryDetailActivity.class);
        notificationIntent.putExtras(intent.getExtras());
        issueNotification(notificationIntent);
    }

    /**
     * Issue a Notification and schedule the next ones for all diary-entries
     *
     * @param notificationIntent The intent which was received from the AlarmManager
     */
    private void issueNotification(Intent notificationIntent) {
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(notificationIntent.getIntExtra(INTENT_EXTRA_NOTIFICATION_ID, DEFAULT_NOTIFICATION_ID), 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(notificationIntent.getStringExtra(INTENT_EXTRA_NOTIFICATION_TITLE))
                .setContentText(notificationIntent.getStringExtra(INTENT_EXTRA_NOTIFICATION_CONTENT_TEXT))
                .setSmallIcon(R.drawable.diary_notification_icon)
                .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationIntent.getIntExtra(INTENT_EXTRA_NOTIFICATION_ID, DEFAULT_NOTIFICATION_ID), notification);

        NotificationService.createOrUpdateNotificationsForActiveEntries(this);
    }
}
