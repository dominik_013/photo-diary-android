package at.fhhagenberg.photo_diary.service;


import android.content.Context;

import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.sql.DatabaseManager;

/**
 * A class managing creation, completion and deletion of diary entries.
 */
public class DiaryEntryService implements IDiaryEntryService {

    /**
     * The single instance of the DiaryEntryService.
     */
    private static DiaryEntryService instance;

    private DiaryEntryService() {
    }

    /**
     * Returns an instance of DiaryEntryService.
     *
     * @return An instance of DiaryEntryService.
     */
    public static synchronized DiaryEntryService getInstance() {
        if (instance == null) {
            instance = new DiaryEntryService();
        }

        return instance;
    }

    @Override
    public void createDiaryEntry(final Context context, DiaryEntry diaryEntry) {
        DatabaseManager.getInstance(context).createOrUpdateDiaryEntry(diaryEntry);
        NotificationService.createOrUpdateNotificationsForActiveEntries(context);
    }

    @Override
    public void completeDiaryEntry(Context context, DiaryEntry diaryEntry) {
        diaryEntry.setManualCompletionFinished(true);
        diaryEntry.setNotificationEnabled(false);
        diaryEntry.setNotificationMode(NotificationService.NotificationMode.NONE);
        diaryEntry.setNextNotification(null);
        NotificationService.removeNotificationsForDiaryEntry(context, diaryEntry);
        DatabaseManager.getInstance(context).createOrUpdateDiaryEntry(diaryEntry);
    }

    @Override
    public void deleteDiaryEntry(Context context, DiaryEntry diaryEntry) {
        NotificationService.removeNotificationsForDiaryEntry(context, diaryEntry);
        DatabaseManager.getInstance(context).deleteDiaryEntry(diaryEntry);
    }
}
