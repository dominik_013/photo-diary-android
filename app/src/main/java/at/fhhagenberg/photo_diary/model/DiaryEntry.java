package at.fhhagenberg.photo_diary.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import org.joda.time.DateTime;

/**
 * Class that represents a diary entry.
 */
@DatabaseTable(tableName = "diaryentry")
public class DiaryEntry extends ModelBaseClass {

    /**
     * The unique ID of the User.
     */
    @Expose
    @DatabaseField
    private long userId;

    /**
     * Determines whether the diary entry is visible only to its creator or not.
     */
    @Expose
    @DatabaseField
    private boolean isPrivate;

    /**
     * The name of the diary entry, which is shown as title, used for stored
     * files and as title of the gallery and finished entries.
     */
    @Expose
    @SerializedName("name")
    @DatabaseField
    private String name;

    /**
     * The note the user entered when creating the diary entry.
     */
    @Expose
    @DatabaseField
    private String note;

    /**
     * The date at which the diary entry started.
     */
    @Expose
    @DatabaseField
    private DateTime from;

    /**
     * The date at which the diary entry is closed.
     */
    @Expose
    @DatabaseField
    private DateTime to;

    /**
     * The Images that are stored in the diary entry.
     */
    @Expose
    @ForeignCollectionField
    private ForeignCollection<Image> images;

    /**
     * The duration per picture when the entry is shown.
     */
    @Expose
    @DatabaseField
    private int durationPerPicture;

    /**
     * Determines whether the entry can be manually completed or not.
     */
    @Expose
    @DatabaseField
    private boolean isManualCompletionActive;

    /**
     * Determines if the entry was already manually completed.
     */
    @Expose
    @DatabaseField
    private boolean isManualCompletionFinished;

    /**
     * Determines if the user receives notifications for this entry.
     */
    @Expose
    @DatabaseField
    private boolean isNotificationEnabled;

    /**
     * Determines the mode (number) of notifications.
     */
    @Expose
    @DatabaseField
    private int notificationMode;

    /**
     * Stores the date and time of the next notification.
     */
    @Expose
    @DatabaseField
    private DateTime nextNotification;

    /* Constructors */

    // No arg constructor is needed for orm-lite
    public DiaryEntry() {
    }

    public DiaryEntry(String name) {
        this.name = name;
    }

    public DiaryEntry(String name, String note, DateTime from, DateTime to, boolean isManualCompletionActive, boolean isNotificationEnabled, int notificationMode) {
        userId = 1;
        isPrivate = false;
        this.name = name;
        this.note = note;
        this.from = from;
        this.to = to;
        this.isManualCompletionActive = isManualCompletionActive;
        this.isManualCompletionFinished = false;
        this.isNotificationEnabled = isNotificationEnabled;
        this.notificationMode = notificationMode;
        this.nextNotification = null;
        durationPerPicture = 2;
    }

    /* Getter and setter */

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public DateTime getFrom() {
        return from;
    }

    public void setFrom(DateTime from) {
        this.from = from;
    }

    public DateTime getTo() {
        return to;
    }

    public void setTo(DateTime to) {
        this.to = to;
    }

    public ForeignCollection<Image> getImages() {
        return images;
    }

    public void setImages(ForeignCollection<Image> images) {
        this.images = images;
    }

    public int getDurationPerPicture() {
        return durationPerPicture;
    }

    public void setDurationPerPicture(int durationPerPicture) {
        this.durationPerPicture = durationPerPicture;
    }

    public boolean isManualCompletionActive() {
        return isManualCompletionActive;
    }

    public void setManualCompletionActive(boolean manualCompletionActive) {
        isManualCompletionActive = manualCompletionActive;
    }

    public boolean isManualCompletionFinished() {
        return isManualCompletionFinished;
    }

    public void setManualCompletionFinished(boolean manualCompletionFinished) {
        isManualCompletionFinished = manualCompletionFinished;
    }

    public boolean isNotificationEnabled() {
        return isNotificationEnabled;
    }

    public void setNotificationEnabled(boolean notificationEnabled) {
        isNotificationEnabled = notificationEnabled;
    }

    public int getNotificationMode() {
        return notificationMode;
    }

    public void setNotificationMode(int notificationMode) {
        this.notificationMode = notificationMode;
    }

    public DateTime getNextNotification() {
        return nextNotification;
    }

    public void setNextNotification(DateTime nextNotification) {
        this.nextNotification = nextNotification;
    }
}
