package at.fhhagenberg.photo_diary.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

/**
 * The baseclass for all models used.
 */
public class ModelBaseClass {

    public static final String ID = "externalId";

    /**
     * The external id of the model instance in the database.
     */
    @Expose
    @SerializedName("id")
    @DatabaseField(generatedId = true)
    private int externalId;

    public int getExternalId() {
        return externalId;
    }

    public void setExternalId(int externalId) {
        this.externalId = externalId;
    }
}
