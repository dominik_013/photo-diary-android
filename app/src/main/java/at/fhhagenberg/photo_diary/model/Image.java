package at.fhhagenberg.photo_diary.model;

import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.joda.time.DateTime;

/**
 * A class that represents a image stored in a diary entry.
 */
@DatabaseTable(tableName = "images")
public class Image extends ModelBaseClass {

    public static final String DIARY_ENTRY_ID_FIELD_NAME = "diary_entry_id";

    /**
     * The diary entry the image belongs to.
     */
    @Expose
    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = DIARY_ENTRY_ID_FIELD_NAME)
    private DiaryEntry diaryEntry;

    /**
     * The path to the file that holds the image.
     */
    @Expose
    @DatabaseField
    private String filePath;

    /**
     * The date and time the image was taken on.
     */
    @Expose
    @DatabaseField
    private DateTime timeTaken;

    // Needed for persisting data
    public Image() {
    }

    public Image(DiaryEntry diaryEntry, String filePath, DateTime timeTaken) {
        this.diaryEntry = diaryEntry;
        this.filePath = filePath;
        this.timeTaken = timeTaken;
    }

    /* Getter and Setter */

    public DiaryEntry getDiaryEntry() {
        return diaryEntry;
    }

    public void setDiaryEntry(DiaryEntry diaryEntry) {
        this.diaryEntry = diaryEntry;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public DateTime getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(DateTime timeTaken) {
        this.timeTaken = timeTaken;
    }
}
