package at.fhhagenberg.photo_diary.ui.navigation;


import android.support.v4.app.Fragment;

public abstract class NavigationFragment extends Fragment {

    protected NavigationActivity getNavigationActivity() {
        return (NavigationActivity) getActivity();
    }

    protected NavigationFragment getFragment(int id) {
        return getNavigationActivity().getNavigationPagerAdapter().getRegisteredFragment(id);
    }

    /**
     * Update the data of the given Fragment, if it gets loaded
     */
    protected abstract void updateData();
}
