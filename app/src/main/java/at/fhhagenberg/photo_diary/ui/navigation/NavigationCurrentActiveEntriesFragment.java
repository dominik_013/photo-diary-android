package at.fhhagenberg.photo_diary.ui.navigation;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.define.Define;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.adapter.ActiveDiaryEntryAdapter;
import at.fhhagenberg.photo_diary.helper.AddPictureResultHandler;
import at.fhhagenberg.photo_diary.helper.CameraHelper;
import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.sql.DatabaseManager;
import at.fhhagenberg.photo_diary.ui.CameraActivity;
import at.fhhagenberg.photo_diary.view.DialogCreator;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NavigationCurrentActiveEntriesFragment extends NavigationFragment implements ActiveDiaryEntryAdapter.AddImageFromCardClickListener {

    @BindView(R.id.recyclerView_current_active_entries)
    protected RecyclerView recyclerView;

    private ActiveDiaryEntryAdapter adapter;
    private DiaryEntry diaryEntryLastAdd;

    private final int REQUEST_PERMISSION_CAMERA = 100;
    private final int REQUEST_ACTIVITY_CAMERA = 101;

    public NavigationCurrentActiveEntriesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_current_active_entries, container, false);
        ButterKnife.bind(this, view);
        adapter = new ActiveDiaryEntryAdapter(getActivity(), this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        //recyclerView.addItemDecoration(new HorizontalDividerItemDecoration(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        adapter.updataListData(DatabaseManager.getInstance(getActivity()).getAllCurrentActiveDiaryEntries());

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Define.ALBUM_REQUEST_CODE) {
            AddPictureResultHandler.onAddFromGalleryResult(getActivity(), diaryEntryLastAdd, resultCode, data);
        }

        /**
         * Crop result is handled in NavigationActivity's onActivityResult
         */
        adapter.updataListData(DatabaseManager.getInstance(getContext()).getAllCurrentActiveDiaryEntries());
    }

    public ActiveDiaryEntryAdapter getAdapter() {
        return adapter;
    }

    public DiaryEntry getDiaryEntryLastAdd() {
        return diaryEntryLastAdd;
    }

    @Override
    protected void updateData() {

    }

    @Override
    public void onAddFromGalleryClick(DiaryEntry diaryEntry) {
        diaryEntryLastAdd = diaryEntry;
        FishBun.with(this)
                .setActionBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimary), ContextCompat.getColor(getContext(), R.color.colorPrimaryDark))
                .setPickerCount(10)
                .setButtonInAlbumActivity(false)
                .startAlbum();
    }

    @Override
    public void onAddFromCameraClick(DiaryEntry diaryEntry) {
        if (!CameraHelper.isCameraHardwareAvailable(getContext())) {
            Toast.makeText(getContext(), getResources().getString(R.string.error_no_camera_available), Toast.LENGTH_SHORT).show();
            return;
        }

        diaryEntryLastAdd = diaryEntry;

        // If the user has not already granted the permission for camera and writing to external storage
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // If the users has chosen the "never ask again" option
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                DialogCreator.showDialogOKCancel(getContext(), getResources().getString(R.string.request_camera_permissions), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA);
                    }
                }, null);
            } else {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA);
            }
        } else {
            startCameraActivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CAMERA: {
                if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    // Permission was granted!
                    startCameraActivity();
                } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // Camera permission denied
                    Toast.makeText(getContext(), getResources().getString(R.string.error_no_camera_permission), Toast.LENGTH_SHORT).show();
                } else if (grantResults.length > 1 && grantResults[1] == PackageManager.PERMISSION_DENIED) {
                    // External storage permission denied
                    Toast.makeText(getContext(), getResources().getString(R.string.error_no_write_permission), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_no_camera_permissions), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void startCameraActivity() {
        Intent intent = new Intent(getActivity(), CameraActivity.class);
        intent.putExtra(ActiveDiaryEntryAdapter.EXTRA_SELECTED_DIARY_ENTRY, diaryEntryLastAdd.getExternalId());
        startActivityForResult(intent, REQUEST_ACTIVITY_CAMERA);
    }
}
