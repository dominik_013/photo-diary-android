package at.fhhagenberg.photo_diary.ui;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.j256.ormlite.dao.ForeignCollection;

import java.io.File;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.adapter.CompletedDiaryEntryAdapter;
import at.fhhagenberg.photo_diary.helper.DiarySettings;
import at.fhhagenberg.photo_diary.helper.GifEncoder;
import at.fhhagenberg.photo_diary.helper.ImageManipulator;
import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.model.Image;
import at.fhhagenberg.photo_diary.sql.DatabaseManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Activity that displays a completed diary entry.
 */
public class CompletedDiaryEntryDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private final static int REQUEST_SHARE_GIF = 1;

    private Animation fadeIn;
    private Animation fadeOut;
    private int currentPosition;
    private boolean playing;

    private int delay;
    private int speed;
    private int quality;

    private DiaryEntry entry;
    private Image[] images;
    private Drawable[] drawables;

    @BindView(R.id.completed_diary_entry_image_switcher)
    protected ImageSwitcher imageSwitcher;

    @BindView(R.id.txt_completed_name)
    protected TextView txtCompletedName;

    @BindView(R.id.completed_diary_previous)
    protected ImageButton btnPrevious;

    @BindView(R.id.completed_diary_play)
    protected ImageButton btnPlay;

    @BindView(R.id.completed_diary_next)
    protected ImageButton btnNext;

    private File sharingGifFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_diary_entry_detail);
        ButterKnife.bind(this);

        speed = DiarySettings.getCompletedDiaryAutoplay(this);
        delay = DiarySettings.getCompletedDiaryGifSpeed(this);
        quality = DiarySettings.getCompletedDiaryExportQuality(this);

        //Get the DiaryEntry instance whose details are displayed
        final int id = getIntent().getIntExtra(CompletedDiaryEntryAdapter.EXTRA_SELECTED_DIARY_ENTRY, -1);

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                entry = DatabaseManager.getInstance(getApplicationContext()).getDiaryEntry(id);

                ForeignCollection<Image> imageData = entry.getImages();
                images = new Image[imageData.size()];
                imageData.toArray(images);

                drawables = imagesToDrawable(images);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                txtCompletedName.setText(entry.getName());
                imageSwitcher.setInAnimation(fadeIn);
                imageSwitcher.setOutAnimation(fadeOut);

                imageSwitcher.setImageDrawable(drawables[currentPosition]);
            }
        }.execute();

        btnPrevious.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        currentPosition = 0;
        playing = true;

        fadeIn = AnimationUtils.loadAnimation(this, R.anim.preview_fade_in);
        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (playing) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (playing) {
                                if (images != null && images.length > 0) {
                                    currentPosition = getNextPosition();
                                    imageSwitcher.setImageDrawable(drawables[currentPosition]);
                                }
                            }
                        }
                    }, speed);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fadeOut = AnimationUtils.loadAnimation(this, R.anim.preview_fade_out);

        //Create views that are displayed in the image switcher
        imageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView view = new ImageView(getApplicationContext());
                view.setAdjustViewBounds(true);
                view.setScaleType(ImageView.ScaleType.FIT_XY);
                view.setLayoutParams(new ImageSwitcher.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT));
                return view;
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SHARE_GIF) {
            if (sharingGifFile != null) {
                if (sharingGifFile.delete()) {
                    Log.i("CompletedDiaryEntryAct", "File deleted");
                }
            }
        }
    }

    @OnClick(R.id.btn_completed_share_gif)
    protected void onShareGifClick() {
        new ShareGifTask(this, images).execute();
    }

    private class ShareGifTask extends AsyncTask<Void, Void, Boolean> {

        private final Activity activity;
        private Image[] images;
        private AlertDialog alertDialog;
        private boolean canceled = false;

        public ShareGifTask(final Activity activity, Image[] images) {
            this.activity = activity;
            this.images = images;
        }

        @Override
        protected void onPreExecute() {
            alertDialog = new AlertDialog.Builder(activity)
                    .setTitle("Gif has to be encoded")
                    .setMessage("The gif-encoding process may take some time, please be patient.")
                    .setNegativeButton(getString(R.string.action_cancel_encoding), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            cancel(true);
                            canceled = true;
                        }
                    })
                    .show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (images.length < 1) {
                return false;
            }
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Log.i("CompletedDiary", "Permission not granted");
            } else {
                Log.i("CompletedDiary", "Permission Granted");
                sharingGifFile = GifEncoder.encode(images, entry.getName(), delay, quality);

                if (sharingGifFile != null) {
                    Log.i("CompletedDiary", "Finished creating gif");
                    return true;
                } else {
                    Log.i("CompletedDiary", "Creating gif failed");
                    return false;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (canceled) {
                return;
            }

            alertDialog.dismiss();
            if (!aBoolean || sharingGifFile == null) {
                Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
            } else {
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("image/gif");
                Uri uri = Uri.fromFile(sharingGifFile);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                startActivityForResult(Intent.createChooser(shareIntent, "Share gif"), REQUEST_SHARE_GIF);
            }
        }
    }

    /**
     * Creates a array of Drawables (BitmapDrawable) from a array of Images
     *
     * @param images The array of Images that has to be converted.
     * @return A array of Drawables that are created from the Images.
     */
    private Drawable[] imagesToDrawable(Image[] images) {
        Drawable[] drawables = new Drawable[images.length];

        for (int i = 0; i < images.length; i++) {
            drawables[i] = new BitmapDrawable(this.getResources(), ImageManipulator.getThumbnailFromFile(images[i].getFilePath(), quality, quality));
            Log.i("CompletedDiary", "drawables: " + drawables[i]);
        }

        return drawables;
    }

    /**
     * Gets the position of the next image to display in the drawable array.
     *
     * @return The position of the next image.
     */
    private int getNextPosition() {
        if (currentPosition + 1 >= images.length) {
            return 0;
        } else {
            return currentPosition + 1;
        }
    }

    /**
     * Gets the position of the previous image to display in the drawable array.
     *
     * @return The position of the previous image.
     */
    private int getPreviousPosition() {
        if (currentPosition - 1 >= 0) {
            return currentPosition - 1;
        } else {
            return images.length - 1;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.completed_diary_previous: {
                playing = false;
                if (images != null && images.length > 0) {
                    currentPosition = getNextPosition();
                    imageSwitcher.setImageDrawable(drawables[currentPosition]);
                    btnPlay.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_play_arrow_white_48dp));
                }
                break;
            }
            case R.id.completed_diary_play: {
                if (playing) {
                    playing = false;
                    btnPlay.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_play_arrow_white_48dp));
                } else {
                    playing = true;
                    currentPosition = getNextPosition();
                    imageSwitcher.setImageDrawable(drawables[currentPosition]);
                    btnPlay.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_pause_white_48dp));
                }
                break;
            }
            case R.id.completed_diary_next: {
                playing = false;
                if (images != null && images.length > 0) {
                    currentPosition = getPreviousPosition();
                    imageSwitcher.setImageDrawable(drawables[currentPosition]);
                    btnPlay.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_play_arrow_white_48dp));
                }
                break;
            }
        }
    }
}
