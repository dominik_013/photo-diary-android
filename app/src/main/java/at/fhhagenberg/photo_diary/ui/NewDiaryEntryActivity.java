package at.fhhagenberg.photo_diary.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.joda.time.DateTime;

import java.util.Calendar;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.helper.TimeHelper;
import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.service.DiaryEntryService;
import at.fhhagenberg.photo_diary.service.NotificationService;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Activity to create a new diary entry.
 */
public class NewDiaryEntryActivity extends AppCompatActivity {

    private final String TAG_DIALOG_DATE_FROM = "datePickerFrom";
    private final String TAG_DIALOG_DATE_TO = "datePickerTo";

    @BindView(R.id.toolbar_new_diary_entry)
    protected Toolbar toolbar;

    @BindView(R.id.edt_new_diary_entry_name)
    protected EditText editTextName;

    @BindView(R.id.edt_new_diary_entry_from)
    protected EditText editTextFrom;

    @BindView(R.id.edt_new_diary_entry_to)
    protected EditText editTextTo;

    @BindView(R.id.edt_new_diary_entry_note)
    protected EditText editTextNote;

    @BindView(R.id.checkbox_new_diary_entry_notifications_enabled)
    protected CheckBox checkBoxNotificationsEnabled;

    @BindView(R.id.spinner_new_diary_entry_notification_option)
    protected Spinner spinnerNotificationOption;

    private DateTime dateTimeFrom = null;
    private DateTime dateTimeTo = null;

    DialogInterface.OnClickListener cancelDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    finish();
                    break;
            }
        }
    };

    private CheckBox.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                spinnerNotificationOption.setVisibility(View.VISIBLE);
            } else {
                spinnerNotificationOption.setVisibility(View.GONE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_diary_entry);
        ButterKnife.bind(this);

        // Disable input for from- and to-date
        editTextFrom.setKeyListener(null);
        editTextTo.setKeyListener(null);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(getResources().getString(R.string.toolbar_title_new_diary_entry));

        spinnerNotificationOption.setVisibility(View.GONE);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.notification_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinnerNotificationOption.setAdapter(adapter);
        checkBoxNotificationsEnabled.setOnCheckedChangeListener(checkedChangeListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_new_diary_entry, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_diary_entry_save:
                if (isInputValid()) {
                    DiaryEntry diaryEntry;
                    if (dateTimeFrom == null && dateTimeTo == null) {
                        diaryEntry = new DiaryEntry(
                                editTextName.getText().toString(),
                                editTextNote.getText().toString(),
                                dateTimeFrom,
                                dateTimeTo,
                                true,
                                checkBoxNotificationsEnabled.isChecked(),
                                checkBoxNotificationsEnabled.isChecked() ? spinnerNotificationOption.getSelectedItemPosition() : NotificationService.NotificationMode.NONE);
                    } else {
                        diaryEntry = new DiaryEntry(
                                editTextName.getText().toString(),
                                editTextNote.getText().toString(),
                                dateTimeFrom, dateTimeTo,
                                false,
                                checkBoxNotificationsEnabled.isChecked(),
                                checkBoxNotificationsEnabled.isChecked() ? spinnerNotificationOption.getSelectedItemPosition() : NotificationService.NotificationMode.NONE);
                    }

                    DiaryEntryService.getInstance().createDiaryEntry(this, diaryEntry);

                    setResult(RESULT_OK);
                    finish();
                } else {
                    if (editTextName.getText().toString().trim().isEmpty()) {
                        editTextName.setError(getResources().getString(R.string.error_new_diary_entry_name_required));
                    }

                    if (dateTimeFrom == null || dateTimeTo == null) {
                        dateTimeFrom = dateTimeTo = null;
                        editTextFrom.setText(null);
                        editTextTo.setText(null);
                        Toast.makeText(this, getResources().getString(R.string.error_new_diary_entry_start_or_end_time), Toast.LENGTH_LONG).show();
                    }
                }
                return true;

            case R.id.menu_item_new_diary_entry_cancel:
                if (isInputValid()) {
                    // Show dialog if the user really wants to cancel
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(getResources().getString(R.string.dialog_cancel_question))
                            .setPositiveButton(getResources().getString(R.string.dialog_cancel_yes), cancelDialogClickListener)
                            .setNegativeButton(getResources().getString(R.string.dialog_cancel_no), cancelDialogClickListener)
                            .show();
                } else {
                    finish();
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Checks if the input is valid (Entry has to have a name and a selected from- and to-date)
     *
     * @return True if the input for the entry is valid
     */
    private boolean isInputValid() {
        if (dateTimeFrom == null && dateTimeTo != null || dateTimeFrom != null && dateTimeTo == null) {
            return false;
        }
        return !(editTextName.getText().toString().trim().isEmpty());
    }

    @OnClick(R.id.edt_new_diary_entry_from)
    protected void onEditTextFromClicked() {
        //Hide Keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);

        //Show date picker
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance(onFromSetListener);
        datePickerFragment.show(getSupportFragmentManager(), TAG_DIALOG_DATE_FROM);
    }

    @OnClick(R.id.edt_new_diary_entry_to)
    protected void onEditTextToClicked() {
        //Hide Keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);

        //Show date picker
        DatePickerFragment datePickerFragment = DatePickerFragment.newInstance(onToSetListener);
        datePickerFragment.show(getSupportFragmentManager(), TAG_DIALOG_DATE_TO);
    }


    /**
     * FromSetListener gets called when the from date was chosen
     * Checks if the selected date is valid (if it is before the to-date) and creates a new date-time object
     */
    DatePickerDialog.OnDateSetListener onFromSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            dateTimeFrom = new DateTime(year, month + 1, dayOfMonth, 0, 0);
            if (dateTimeTo != null && dateTimeTo.isBefore(dateTimeFrom)) {
                dateTimeFrom = null;
                editTextFrom.setText(null);
                Toast.makeText(getApplicationContext(), "Date has to be before to-date", Toast.LENGTH_SHORT).show();
            } else {
                editTextFrom.setText(TimeHelper.dateTimeToReadableDate(getApplicationContext(), dateTimeFrom));
            }
        }
    };


    /**
     * ToSetListener gets called when the to date was chosen
     * Checks if the selected date is valid (if it is after the from-date) and creates a new date-time object
     */
    DatePickerDialog.OnDateSetListener onToSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            dateTimeTo = new DateTime(year, month + 1, dayOfMonth, 23, 59, 59);
            if (dateTimeFrom != null && dateTimeFrom.isAfter(dateTimeTo)) {
                dateTimeTo = null;
                editTextTo.setText(null);
                Toast.makeText(getApplicationContext(), "Date has to be after from-date", Toast.LENGTH_SHORT).show();
            } else {
                editTextTo.setText(TimeHelper.dateTimeToReadableDate(getApplicationContext(), dateTimeTo));
            }
        }
    };

    /**
     * Class to create a date picker dialog when creating a new entry.
     */
    public static class DatePickerFragment extends DialogFragment {

        private DatePickerDialog.OnDateSetListener onDateSetListener;

        static DatePickerFragment newInstance(DatePickerDialog.OnDateSetListener onDateSetListener) {
            DatePickerFragment pickerFragment = new DatePickerFragment();
            pickerFragment.setOnDateSetListener(onDateSetListener);

            return pickerFragment;
        }

        @Override
        public
        @NonNull
        Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getContext(), onDateSetListener, year, month, day);
        }

        private void setOnDateSetListener(DatePickerDialog.OnDateSetListener listener) {
            this.onDateSetListener = listener;
        }
    }
}
