package at.fhhagenberg.photo_diary.ui.onboarding;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.client.HttpStatus;
import at.fhhagenberg.photo_diary.client.RestClient;
import at.fhhagenberg.photo_diary.client.UserService;
import at.fhhagenberg.photo_diary.helper.DiarySettings;
import at.fhhagenberg.photo_diary.model.User;
import at.fhhagenberg.photo_diary.ui.navigation.NavigationActivity;
import at.fhhagenberg.photo_diary.view.LoginDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnboardingPersonalizationFragment extends OnboardingFragment {

    @BindView(R.id.edt_onboarding_personalization_first_name)
    protected EditText edtFirstName;
    @BindView(R.id.edt_onboarding_personalization_last_name)
    protected EditText edtLastName;
    @BindView(R.id.edt_onboarding_personalization_email)
    protected EditText edtEmail;
    @BindView(R.id.edt_onboarding_personalization_password)
    protected EditText edtPassword;


    Callback<User> manualRegisterCallback = new Callback<User>() {
        @Override
        public void onResponse(Call<User> call, Response<User> response) {
            if (response.isSuccessful()) {
                onLoginSuccessful(edtEmail.getText().toString(), edtPassword.getText().toString(), edtFirstName.getText().toString(), edtLastName.getText().toString(), true);
            } else if (response.code() == HttpStatus.INTERNAL_SERVER_ERROR) {
                Toast.makeText(getActivity(), getResources().getString(R.string.error_email_already_in_use), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<User> call, Throwable t) {
            Toast.makeText(getContext(), "Failed to register user", Toast.LENGTH_SHORT).show();
            Log.e("Failed request", t.getMessage());
        }
    };

    public OnboardingPersonalizationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_onboarding_personalization, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected boolean isValid() {
        return !edtEmail.getText().toString().trim().isEmpty() &&
                !edtPassword.getText().toString().trim().isEmpty();
    }

    @Override
    protected void saveSettings() {
        UserService userService = RestClient.createService(UserService.class);
        Call<User> call = userService.registerUser(new User(
                edtFirstName == null ? "" : edtFirstName.getText().toString(),
                edtLastName == null ? "" : edtLastName.getText().toString(),
                edtEmail.getText().toString(),
                edtPassword.getText().toString()));
        call.enqueue(manualRegisterCallback);
    }

    @OnClick(R.id.txt_btn_onboarding_personalization_login)
    protected void onLoginClick() {
        LoginDialog dialog = new LoginDialog();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        dialog.show(fragmentManager, "login_dialog");
    }

    @OnClick(R.id.txt_btn_onboarding_personalization_stay_offline)
    protected void onStayOfflineClick() {
        String firstName = edtFirstName.getText().toString();
        String lastName = edtLastName.getText().toString();
        onLoginSuccessful("", "",
                firstName.isEmpty() ? getResources().getString(R.string.text_user_first_name_placeholder) : firstName,
                lastName.isEmpty() ? "" : lastName,
                false);
    }

    private void onLoginSuccessful(String email, String password, String firstName, String lastName, boolean isLoggedIn) {
        DiarySettings.setOnboardingCompleted(getContext(), true);
        DiarySettings.setPersonalizationEmail(getContext(), email);
        // TODO: DON'T STORE PASSWORD IN SHARED PREFERENCES ! ! !
        DiarySettings.setPersonalizationPassword(getContext(), password);
        DiarySettings.setPersonalizationFirstName(getContext(), firstName);
        DiarySettings.setPersonalizationLastName(getContext(), lastName);
        DiarySettings.setIsLoggedIn(getContext(), isLoggedIn);

        startActivity(new Intent(getContext(), NavigationActivity.class));
        getActivity().finish();
    }
}
