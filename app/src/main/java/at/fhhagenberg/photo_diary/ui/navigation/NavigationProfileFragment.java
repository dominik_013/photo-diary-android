package at.fhhagenberg.photo_diary.ui.navigation;


import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;

import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.define.Define;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.List;
import java.util.Locale;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.adapter.CompletedDiaryEntryAdapter;
import at.fhhagenberg.photo_diary.helper.CircleTransform;
import at.fhhagenberg.photo_diary.helper.DiarySettings;
import at.fhhagenberg.photo_diary.helper.ImageManipulator;
import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.sql.DatabaseManager;
import at.fhhagenberg.photo_diary.ui.decoration.HorizontalDividerItemDecoration;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;


public class NavigationProfileFragment extends NavigationFragment implements AppBarLayout.OnOffsetChangedListener {

    private final static int MIN_SEARCH_CHARACTERS = 2;
    private final static int REQUEST_SELECT_PROFILE_PICTURE = 1;
    private final static int REQUEST_PROFILE_PICTURE_CROPPED = 2;

    @BindView(R.id.appbar_layout_profile)
    protected AppBarLayout appBarLayout;

    @BindView(R.id.collapsing_toolbar_layout_profile)
    protected CollapsingToolbarLayout toolbarLayout;

    @BindView(R.id.toolbar_profile)
    protected Toolbar toolbar;

    @BindView(R.id.image_profile_profilePicture)
    protected ImageButton imageProfilePicture;

    @BindView(R.id.txt_profile_name)
    protected TextView textViewName;

    @BindView(R.id.txt_profile_number_of_entries)
    protected TextView textViewNumberOfEntries;

    @BindView(R.id.recyclerView_profile_entries)
    protected RecyclerView recyclerView;

    public NavigationProfileFragment() {
    }

    private List<DiaryEntry> diaryEntryList;
    private CompletedDiaryEntryAdapter diaryEntryAdapter;
    private State currentState = State.EXPANDED;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_profile, container, false);
        ButterKnife.bind(this, view);

        setHasOptionsMenu(true);

        diaryEntryList = DatabaseManager.getInstance(getContext()).getAllFinishedDiaryEntries();
        diaryEntryAdapter = new CompletedDiaryEntryAdapter(getActivity());

        appBarLayout.addOnOffsetChangedListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(diaryEntryAdapter);
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration(getContext()));

        diaryEntryAdapter.updateListData(diaryEntryList);

        File file = new File(DiarySettings.getProfilePicturePath(getContext()));

        if (file.exists() && !file.isDirectory()) {
            Picasso.with(getActivity())
                    .load(file)
                    .transform(new CircleTransform())
                    .resizeDimen(R.dimen.profile_picture_size, R.dimen.profile_picture_size)
                    .centerCrop()
                    .into(imageProfilePicture);
        } else {
            Picasso.with(getActivity())
                    .load(R.drawable.profile_picture_default)
                    .transform(new CircleTransform())
                    .resizeDimen(R.dimen.profile_picture_size, R.dimen.profile_picture_size)
                    .centerCrop()
                    .into(imageProfilePicture);
        }

        textViewName.setText(String.format(Locale.getDefault(), "%s %s",
                DiarySettings.getPersonalizationFirstName(getContext()),
                DiarySettings.getPersonalizationLastName(getContext())));

        int numberOfEntries = diaryEntryList.size();
        textViewNumberOfEntries.setText(getActivity().getResources().getQuantityString(R.plurals.text_profile_entry_count, numberOfEntries, numberOfEntries));

        toolbar.setTitle("");


        return view;
    }

    public synchronized void refreshCompletedEntryList() {

        DatabaseManager.getInstance(getContext()).deleteEmptyFinishedEntries();

        diaryEntryList = DatabaseManager.getInstance(getContext()).getAllFinishedDiaryEntries();
        diaryEntryAdapter.updateListData(diaryEntryList);
        int numberOfEntries = diaryEntryList.size();
        textViewNumberOfEntries.setText(getActivity().getResources().getQuantityString(R.plurals.text_profile_entry_count, numberOfEntries, numberOfEntries));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        setupSearch(menu);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SELECT_PROFILE_PICTURE && resultCode == RESULT_OK) {
            Log.i("se", "lected");

            List<String> paths = data.getStringArrayListExtra(Define.INTENT_PATH);

            if (paths == null || paths.size() < 1) {
                return;
            }

            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
            options.setToolbarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            options.setActiveWidgetColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));

            UCrop.of(Uri.fromFile(new File(paths.get(0))), Uri.fromFile(ImageManipulator.getAvailableProfilePictureFile()))
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(1080, 1080)
                    .withOptions(options)
                    .start(getContext(), this, REQUEST_PROFILE_PICTURE_CROPPED);
        }

        if (requestCode == REQUEST_PROFILE_PICTURE_CROPPED && resultCode == RESULT_OK) {
            final Uri resultUri = UCrop.getOutput(data);

            // Something that should not occur happened!
            if (resultUri == null) {
                throw new NullPointerException();
            }

            Picasso.with(getActivity())
                    .load(resultUri)
                    .transform(new CircleTransform())
                    .resizeDimen(R.dimen.profile_picture_size, R.dimen.profile_picture_size)
                    .centerCrop()
                    .into(imageProfilePicture);
            DiarySettings.setProfilePicturePath(getContext(), resultUri.getPath());
        }
    }

    @Override
    protected void updateData() {
        textViewName.setText(String.format(Locale.getDefault(), "%s %s",
                DiarySettings.getPersonalizationFirstName(getContext()),
                DiarySettings.getPersonalizationLastName(getContext())));
    }

    public void setupSearch(final Menu menu) {
        MenuItem searchItem = menu.findItem(R.id.menu_item_profile_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(getString(R.string.search_hint));

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                handleSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                handleSearch(newText);
                return false;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return true;
            }
        });
    }

    private synchronized void handleSearch(String searchText) {
        diaryEntryList = DatabaseManager.getInstance(getContext()).getAllMatchingDiaryEntries("%" + searchText + "%");
        diaryEntryAdapter.updateListData(diaryEntryList);
        int numberOfEntries = diaryEntryList.size();
        textViewNumberOfEntries.setText(getActivity().getResources().getQuantityString(R.plurals.text_profile_entry_count, numberOfEntries, numberOfEntries));
    }

    private void stopSearch() {

    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
            if (currentState != State.COLLAPSED) {
                currentState = State.COLLAPSED;
                toolbarLayout.setTitle(getResources().getString(R.string.toolbar_title_profile));
                Log.i("FragmentProfile", "Collapsed");
                textViewName.setVisibility(View.INVISIBLE);
                textViewNumberOfEntries.setVisibility(View.INVISIBLE);
            }
        } else {
            if (currentState != State.EXPANDED) {
                currentState = State.EXPANDED;
                toolbarLayout.setTitle("");
                Log.i("FragmentProfile", "Expanded");
                textViewName.setVisibility(View.VISIBLE);
                textViewNumberOfEntries.setVisibility(View.VISIBLE);
            }
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    /**
     * Selects an profile picture from the gallery
     */
    @OnClick(R.id.image_profile_profilePicture)
    protected void onProfilePictureClick() {
        FishBun.with(this)
                .setActionBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimary), ContextCompat.getColor(getContext(), R.color.colorPrimaryDark))
                .setRequestCode(REQUEST_SELECT_PROFILE_PICTURE)
                .setPickerCount(1)
                .setButtonInAlbumActivity(false)
                .startAlbum();
    }

    private enum State {
        EXPANDED,
        COLLAPSED
    }
}
