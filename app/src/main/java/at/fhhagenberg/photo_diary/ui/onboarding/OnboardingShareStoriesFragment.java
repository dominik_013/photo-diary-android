package at.fhhagenberg.photo_diary.ui.onboarding;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import at.fhhagenberg.photo_diary.R;

public class OnboardingShareStoriesFragment extends OnboardingFragment {

    public OnboardingShareStoriesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_onboarding_share_stories, container, false);
    }
}
