package at.fhhagenberg.photo_diary.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.j256.ormlite.dao.ForeignCollection;

import org.joda.time.DateTime;

import java.io.File;
import java.util.List;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.adapter.ActiveDiaryEntryAdapter;
import at.fhhagenberg.photo_diary.helper.CameraHelper;
import at.fhhagenberg.photo_diary.helper.DiarySettings;
import at.fhhagenberg.photo_diary.helper.ImageManipulator;
import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.model.Image;
import at.fhhagenberg.photo_diary.sql.DatabaseManager;
import at.fhhagenberg.photo_diary.view.CameraPreview;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressWarnings("deprecation")
public class CameraActivity extends AppCompatActivity implements View.OnTouchListener {

    private Camera camera = null;
    private CameraPreview cameraPreview = null;
    private CameraOrientationListener orientationListener;
    private int cameraId;
    private String cameraFlashMode;
    private boolean canTakePicture = true;
    private boolean isHighResolutionSavingEnabled = false;

    private float x1, x2;
    private float y1, y2;
    static final int MIN_DISTANCE = 250;
    private int currentFilter = -1;

    @BindView(R.id.toolbar_camera)
    protected Toolbar toolbar;

    @BindView(R.id.camera_preview)
    protected FrameLayout frameLayoutPreview;

    @BindView(R.id.imageView_camera_thumbnail)
    protected ImageView imageViewThumbnail;

    @BindView(R.id.imageView_freeze_effect)
    protected ImageView imageViewFreezeEffect;

    @BindView(R.id.btn_camera_facing_direction)
    protected ImageButton imageButtonFacingDirection;

    @BindView(R.id.btn_camera_capture)
    protected ImageButton imageButtonCapture;

    @BindView(R.id.btn_camera_flash)
    protected ImageButton imageButtonFlash;

    @BindView(R.id.cameraTopBar)
    protected RelativeLayout topBar;

    @BindView(R.id.cameraBottomBar)
    protected RelativeLayout bottomBar;

    @BindView(R.id.cameraPreviewSizeSquare)
    protected LinearLayout previewSquare;

    @BindView(R.id.activity_camera)
    protected RelativeLayout relativeLayout;

    private DiaryEntry diaryEntry;

    /**
     * This callback is going to be called when the capture button was
     * pressed and a picture was made
     * We save the picture to the file-system and create a database-entry for the image
     */
    private Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            final int rotation = getPhotoRotation();

            new AsyncTask<byte[], Void, Bitmap>() {

                @Override
                protected Bitmap doInBackground(byte[]... params) {

                    File highResFile = ImageManipulator.getAvailableHiResOutputFile();
                    File compressedFile = ImageManipulator.getAvailableCompressedOutputFile();

                    if (highResFile == null) {
                        Log.d("CameraActivity", "Error creating media file!");
                        return null;
                    }

                    // Check if the user is saving high resolution pictures
                    if (isHighResolutionSavingEnabled) {
                        Log.i("CameraActivity", "Saving High Res Image");
                        ImageManipulator.saveHighResolutionImage(highResFile, params[0], rotation);
                    }

                    // Compress the image and save it in a separate folder
                    Log.i("CameraActivity", "Saving Compressed Image");
                    Log.i("CameraActivity", compressedFile.toString());
                    ImageManipulator.saveCompressedImage(compressedFile, params[0], rotation);

                    // Create the database entry for the image
                    if (diaryEntry != null) {
                        Image image = new Image(diaryEntry, compressedFile.getPath(), DateTime.now());
                        DatabaseManager.getInstance(getApplicationContext()).createImage(image);
                        setResult(RESULT_OK);
                    }

                    Bitmap img2 = ImageManipulator.getThumbnailFromByteArray(params[0], 256, 256, rotation);
                    img2 = ImageManipulator.cutBitmapToSquare(img2);

                    return img2;

                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    super.onPostExecute(bitmap);

                    if (bitmap != null) {
                        imageViewThumbnail.setImageBitmap(bitmap);
                    }
                }
            }.execute(data);

            canTakePicture = true;
            camera.startPreview();
        }
    };

    /**
     * Gets called just before a picture was taken by the camera
     * Plays shutter sound
     */
    private Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        @Override
        public void onShutter() {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        ButterKnife.bind(this);


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        //Sets the height of the square to the width of the screen,
        //so it becomes a square.
        previewSquare.getLayoutParams().height = size.x;

        diaryEntry = DatabaseManager.getInstance(this).getDiaryEntry(getIntent().getIntExtra(ActiveDiaryEntryAdapter.EXTRA_SELECTED_DIARY_ENTRY, -1));
        orientationListener = new CameraOrientationListener(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(diaryEntry.getName());
        }

        // Read the persisted data
        cameraId = CameraHelper.obtainCameraId(this);
        cameraFlashMode = CameraHelper.obtainFlashMode(this);
        isHighResolutionSavingEnabled = DiarySettings.isHighResolutionSavingEnabled(this);

        // Update the user interface to the settings
        updateCameraFlashUi();
        updateCameraFacingDirectionUi();

        previewSquare.setOnTouchListener(this);

        //Set the image that was added last to the diary entry as thumbnail
        ForeignCollection<Image> tempImg = diaryEntry.getImages();
        Image[] images;
        if (tempImg != null && tempImg.size() > 0) {
            images = new Image[tempImg.size()];
            images = tempImg.toArray(images);
            imageViewThumbnail.setImageBitmap(ImageManipulator.getThumbnailFromFile(images[images.length - 1].getFilePath(), 256, 256));
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        startCamera();
        canTakePicture = true;

        //Set color effect that was set before
        Camera.Parameters parameters = camera.getParameters();
        List<String> effects = parameters.getSupportedColorEffects();
        if (effects != null && effects.size() > 0) {
            if (currentFilter < 0) {
                currentFilter = effects.indexOf(Camera.Parameters.EFFECT_NONE);
                Log.i("CameraFilter", "Set filter to: " + currentFilter);
            }

            if (currentFilter >= 0 && currentFilter < effects.size()) {
                parameters.setColorEffect(effects.get(currentFilter));
                camera.setParameters(parameters);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        CameraHelper.storeFlashMode(this, cameraFlashMode);
        CameraHelper.storeCameraId(this, cameraId);
        releaseCamera();
    }

    /**
     * Takes a picture
     */
    @OnClick(R.id.btn_camera_capture)
    protected void onCameraCaptureClick() {
        if (camera != null && canTakePicture) {
            camera.takePicture(shutterCallback, null, pictureCallback);
            canTakePicture = false;
        }
    }

    /**
     * Switches between front facing and back facing camera
     * If no front facing camera is available, it just resets the preview for now
     */
    @OnClick(R.id.btn_camera_facing_direction)
    protected void onCameraFacingDirectionClick() {
        if (cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            cameraId = CameraHelper.getBackCameraID();
        } else {
            cameraId = CameraHelper.getFrontCameraIDIfAvailable(getApplicationContext());
        }
        updateCameraFacingDirectionUi();
        restartCamera();
    }

    /**
     * Switches between the available flash-modes (On, Off, Auto)
     */
    @OnClick(R.id.btn_camera_flash)
    protected void onCameraFlashClick() {
        if (!CameraHelper.isFlashAvailable(this)) {
            Toast.makeText(this, getResources().getString(R.string.error_no_camera_flash_available), Toast.LENGTH_SHORT).show();
            return;
        }

        if (cameraFlashMode.equalsIgnoreCase(Camera.Parameters.FLASH_MODE_AUTO)) {
            cameraFlashMode = Camera.Parameters.FLASH_MODE_ON;
        } else if (cameraFlashMode.equalsIgnoreCase(Camera.Parameters.FLASH_MODE_ON)) {
            cameraFlashMode = Camera.Parameters.FLASH_MODE_OFF;
        } else if (cameraFlashMode.equalsIgnoreCase(Camera.Parameters.FLASH_MODE_OFF)) {
            cameraFlashMode = Camera.Parameters.FLASH_MODE_AUTO;
        }

        updateCameraFlashUi();
        updateCameraParameters();
    }

    /**
     * If there are already images in this diary entry, the GalleryDetailsActivity is opened
     */
    @OnClick(R.id.imageView_camera_thumbnail)
    protected void onImageViewThumbnailClick() {
        //Thumbnail is only null if there are no images in the current entry
        if (imageViewThumbnail.getDrawable() != null) {
            Log.i("CameraActivity", "onImageViewThumbnailClick");
            Intent i = new Intent(this, GalleryDetailActivity.class);
            i.putExtra(GalleryDetailActivity.EXTRA_GALLERY_POSITION, GalleryDetailActivity.POSITION_LAST);
            i.putExtra(GalleryDetailActivity.EXTRA_ENTRY_ID, diaryEntry.getExternalId());
            this.startActivity(i);
        }
    }

    /**
     * Returns the rotation when the image was taken to normalize it
     *
     * @return The rotation in degree
     */
    private int getPhotoRotation() {
        int rotation;
        int orientation = orientationListener.getSavedNormalOrientation();
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            rotation = (info.orientation - orientation + 360) % 360;
        } else {
            rotation = (info.orientation + orientation) % 360;
        }

        return rotation;
    }

    /**
     * Starting the camera with the available parameters
     */
    private void startCamera() {
        if (camera == null) {
            try {
                camera = Camera.open(cameraId);
                updateCameraParameters();

                cameraPreview = new CameraPreview(this, camera);
                frameLayoutPreview.addView(cameraPreview);
            } catch (Exception ex) {
                Log.e("DiaryEntryDetail", ex.getMessage());
            }
        }
    }

    /**
     * The camera has to be released, so that other applications can use the hardware
     * when we are not using it
     */
    private void releaseCamera() {
        if (camera != null) {
            camera.release();
            camera = null;
            frameLayoutPreview.removeView(cameraPreview);
            cameraPreview = null;
        }
    }

    /**
     * The camera has to be restarted if we switch the facing direction
     */
    private void restartCamera() {
        if (camera != null) {
            releaseCamera();
        }

        startCamera();
    }

    /**
     * This method is updating the cameras parameters
     * The parameters updated are:  > Picture Size
     * > Preview Size
     * > Flash Mode
     * > Focus Mode
     */
    private void updateCameraParameters() {
        if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            Camera.Size previewSize = CameraHelper.determineBestPreviewSize(parameters, size.x, size.y);
            Camera.Size pictureSize = CameraHelper.determineBestPictureSize(parameters);

            parameters.setPreviewSize(previewSize.width, previewSize.height);
            parameters.setPictureSize(pictureSize.width, pictureSize.height);

            Log.i("CameraActivity", "Set preview size to " + previewSize.width + " x " + previewSize.height);
            Log.i("CameraActivity", "Set picture size to " + pictureSize.width + " x " + pictureSize.height);

            final List<String> flashModes = parameters.getSupportedFlashModes();

            if (flashModes != null && flashModes.contains(cameraFlashMode)) {
                parameters.setFlashMode(cameraFlashMode);
            }

            if (CameraHelper.isContinuousFocusSupported(parameters)) {
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }

            camera.setParameters(parameters);
        }
    }

    /**
     * Depending on the cameraFlashMode we set the UI for the flash-button
     */
    private void updateCameraFlashUi() {
        if (cameraFlashMode.equalsIgnoreCase(Camera.Parameters.FLASH_MODE_AUTO)) {
            imageButtonFlash.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_flash_auto_black_24dp));
        } else if (cameraFlashMode.equalsIgnoreCase(Camera.Parameters.FLASH_MODE_ON)) {
            imageButtonFlash.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_flash_on_black_24dp));
        } else if (cameraFlashMode.equalsIgnoreCase(Camera.Parameters.FLASH_MODE_OFF)) {
            imageButtonFlash.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_flash_off_black_24dp));
        }
    }

    /**
     * Depending on the cameraId we set the UI for the switch-facing-direction button
     */
    private void updateCameraFacingDirectionUi() {
        if (cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            imageButtonFacingDirection.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_camera_front_black_24dp));
        } else if (cameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
            imageButtonFacingDirection.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_camera_rear_black_24dp));
        }
    }

    /**
     * If the user swipes on the camera preview to the left or right, we change the filter that is applied
     *
     * @param v     The view on which the event happened
     * @param event The touch event containing all relevant data
     * @return Returns false if the camera object is null or the current filter is negative, else true
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (camera == null || currentFilter < 0) {
            return false;
        }
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                y1 = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                y2 = event.getY();
                float deltaX = x2 - x1;
                float deltaY = y2 - y1;

                if (Math.abs(deltaX) > MIN_DISTANCE && Math.abs(deltaY) < MIN_DISTANCE) {
                    // Left to Right swipe action
                    if (x2 > x1) {
                        Camera.Parameters parameters = camera.getParameters();
                        List<String> effects = parameters.getSupportedColorEffects();
                        if (effects != null && effects.size() > 0) {
                            if (currentFilter <= 0) {
                                currentFilter = effects.size() - 1;
                            } else {
                                currentFilter--;
                            }
                            parameters.setColorEffect(effects.get(currentFilter));
                            camera.setParameters(parameters);
                        }
                    }

                    // Right to left swipe action
                    else {
                        Camera.Parameters parameters = camera.getParameters();
                        List<String> effects = parameters.getSupportedColorEffects();
                        if (effects != null && effects.size() > 0) {
                            if (currentFilter >= (effects.size() - 1)) {
                                currentFilter = 0;
                            } else {
                                currentFilter++;
                            }
                            parameters.setColorEffect(effects.get(currentFilter));
                            camera.setParameters(parameters);
                        }
                    }

                }

                break;
        }
        return true;
    }

    /**
     * This listener checks when the camera is rotated, so we can then normalize our image
     */
    private static class CameraOrientationListener extends OrientationEventListener {

        private int currentNormalizedOrientation;
        private int savedNormalOrientation;

        CameraOrientationListener(Context context) {
            super(context, SensorManager.SENSOR_DELAY_NORMAL);
        }

        @Override
        public void onOrientationChanged(int i) {
            if (i != ORIENTATION_UNKNOWN) {
                currentNormalizedOrientation = normalize(i);
            }
        }

        private int normalize(int degrees) {
            if (degrees > 315 || degrees <= 45) {
                return 0;
            }

            if (degrees > 45 && degrees <= 135) {
                return 90;
            }

            if (degrees > 135 && degrees <= 225) {
                return 180;
            }

            if (degrees > 225 && degrees <= 315) {
                return 270;
            }

            throw new RuntimeException("This should not have happened");
        }

        void saveOrientation() {
            savedNormalOrientation = currentNormalizedOrientation;
        }

        int getSavedNormalOrientation() {
            saveOrientation();
            return savedNormalOrientation;
        }
    }
}
