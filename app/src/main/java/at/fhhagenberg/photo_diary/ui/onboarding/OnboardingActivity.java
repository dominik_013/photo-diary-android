package at.fhhagenberg.photo_diary.ui.onboarding;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ogaclejapan.smarttablayout.SmartTabLayout;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.adapter.OnboardingPagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnPageChange;

public class OnboardingActivity extends AppCompatActivity {

    private static final int NUM_ONBOARDING_PAGES = 4;
    private final int NAV_FORWARD = 1;
    private final int NAV_BACKWARDS = -1;

    @BindView(R.id.viewpager_onboarding)
    protected ViewPager viewPager;
    @BindView(R.id.viewpager_onboarding_tabs)
    protected SmartTabLayout viewPagerTabLayout;
    private OnboardingPagerAdapter pagerAdapter;

    @BindView(R.id.btn_onboarding_next)
    protected Button btnNext;
    @BindView(R.id.btn_onboarding_back)
    protected Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        ButterKnife.bind(this);

        setupViewPager();
        onPageChanged();
    }

    @OnClick(R.id.btn_onboarding_back)
    protected void onClickBack() {
        changePage(NAV_BACKWARDS);
    }

    /**
     * This method is setting the viewpager to the next page, if we are on the last page, the changes are saved
     * and the onboarding will be closed.
     */
    @OnClick(R.id.btn_onboarding_next)
    protected void onClickNext() {
        changePage(NAV_FORWARD);
    }

    private Toast toastFinishError;

    public void changePage(int navigationDirection) {
        int oldIndex = viewPager.getCurrentItem();
        boolean navigate = true;

        if (navigationDirection == NAV_FORWARD) {
            if (oldIndex == NUM_ONBOARDING_PAGES - 1) {
                navigate = false;
                if (getFragment(oldIndex).isValid()) {
                    getFragment(oldIndex).saveSettings();
                } else {
                    if (toastFinishError != null) {
                        toastFinishError.cancel();
                    }
                    toastFinishError = Toast.makeText(this, "Invalid data", Toast.LENGTH_SHORT);
                    toastFinishError.show();
                    return;
                }
            }
        }

        if (navigationDirection == NAV_BACKWARDS && oldIndex == 0) {
            navigate = false;
        }

        if (navigate) {
            viewPager.setCurrentItem(oldIndex + navigationDirection);
        }
    }

    /**
     * Defines the behavior what happens when the viewpager page gets changed
     * for example we are setting the next/back arrows visibility
     */
    @OnPageChange(R.id.viewpager_onboarding)
    protected void onPageChanged() {
        int idx = viewPager.getCurrentItem();

        if (idx == 0) {
            btnBack.setVisibility(View.GONE);
        } else {
            btnBack.setVisibility(View.VISIBLE);
        }

        if (idx == NUM_ONBOARDING_PAGES - 1) {
            btnNext.setText(getResources().getString(R.string.btn_onboarding_next_finish));
        } else {
            btnNext.setText(getResources().getString(R.string.btn_onboarding_next));
        }
    }

    private void setupViewPager() {
        pagerAdapter = new OnboardingPagerAdapter(getSupportFragmentManager(), NUM_ONBOARDING_PAGES);
        viewPager.setAdapter(pagerAdapter);
        viewPagerTabLayout.setViewPager(viewPager);
    }

    public OnboardingPagerAdapter getOnboardingPagerAdapter() {
        return pagerAdapter;
    }

    private OnboardingFragment getCurrentFragment() {
        return pagerAdapter.getRegisteredFragment(viewPager.getCurrentItem());
    }

    private OnboardingFragment getFragment(int index) {
        return pagerAdapter.getRegisteredFragment(index);
    }
}
