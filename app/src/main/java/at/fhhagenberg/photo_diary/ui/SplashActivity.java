package at.fhhagenberg.photo_diary.ui;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.security.MessageDigest;

import at.fhhagenberg.photo_diary.helper.DiarySettings;
import at.fhhagenberg.photo_diary.ui.navigation.NavigationActivity;
import at.fhhagenberg.photo_diary.ui.onboarding.OnboardingActivity;

/**
 * This Activity is showing the logo on start up and doing the initial decision, if we have to go to the onboarding
 * or if we want to load some data first.
 * The analytic tool from the facebook sdk will also be loaded here, so we can view app statistics (installs, interactions)
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Enable facebook analytics
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplication());

        Intent startIntent;
        if (!DiarySettings.isOnboardingComplete(this)) {
            startIntent = new Intent(this, OnboardingActivity.class);
        } else {
            startIntent = new Intent(this, NavigationActivity.class);
        }
        startActivity(startIntent);
        finish();
    }
}
