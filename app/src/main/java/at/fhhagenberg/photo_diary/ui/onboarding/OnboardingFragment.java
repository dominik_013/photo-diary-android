package at.fhhagenberg.photo_diary.ui.onboarding;


import android.support.v4.app.Fragment;

public abstract class OnboardingFragment extends Fragment {

    protected boolean isValid() {
        return true;
    }

    protected void changePage(final int navigationDirection) {
        if (getActivity() instanceof OnboardingActivity) {
            ((OnboardingActivity) getActivity()).changePage(navigationDirection);
        }
    }

    protected OnboardingFragment getFragment(int id) {
        if (getActivity() instanceof OnboardingActivity) {
            return ((OnboardingActivity) getActivity()).getOnboardingPagerAdapter().getRegisteredFragment(id);
        } else {
            return null;
        }
    }

    protected OnboardingActivity getOnboardingActivity() {
        return (OnboardingActivity) getActivity();
    }

    /**
     * Saves settings specific for the particular fragment
     * e.g. for the personalization fragment first and last-name
     */
    protected void saveSettings() {

    }
}