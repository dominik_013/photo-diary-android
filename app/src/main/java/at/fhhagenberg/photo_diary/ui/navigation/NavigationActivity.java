package at.fhhagenberg.photo_diary.ui.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.yalantis.ucrop.UCrop;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.adapter.ActiveDiaryEntryAdapter;
import at.fhhagenberg.photo_diary.adapter.NavigationPagerAdapter;
import at.fhhagenberg.photo_diary.helper.AddPictureResultHandler;
import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.sql.DatabaseManager;
import at.fhhagenberg.photo_diary.ui.NewDiaryEntryActivity;
import at.fhhagenberg.photo_diary.ui.preference.SettingsActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnPageChange;

public class NavigationActivity extends AppCompatActivity {

    private final static int NUM_NAVIGATION_PAGES = 3;

    private final int REQUEST_NEW_DIARY_ENTRY = 1;
    private final int REQUEST_SETTINGS = 2;

    private final int PAGE_CURRENT_ACTIVE_ENTRIES = 1;
    private final int PAGE_PROFILE = 2;

    @BindView(R.id.toolbar_navigation)
    protected Toolbar toolbar;

    @BindView(R.id.viewpager_navigation)
    protected ViewPager viewPager;
    @BindView(R.id.viewpager_navigation_tabs)
    protected SmartTabLayout viewPagerTabLayout;
    private ImageView[] viewPagerTabs = new ImageView[NUM_NAVIGATION_PAGES];

    private NavigationPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        setupViewpager();
        pageChanged(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        switch (viewPager.getCurrentItem()) {
            case 0:
                toolbar.setTitle(getResources().getString(R.string.toolbar_title_feed));
                getMenuInflater().inflate(R.menu.menu_default, menu);
                break;
            case 1:
                toolbar.setTitle(getResources().getString(R.string.toolbar_title_active_entries));
                getMenuInflater().inflate(R.menu.menu_current_entries, menu);
                break;
            case 2:
                toolbar.setTitle(getResources().getString(R.string.toolbar_title_user_profile));
                //getMenuInflater().inflate(R.menu.menu_profile, menu);
                //((NavigationProfileFragment) getCurrentFragment()).setupSearch(menu);
                return false;
            default:
                getMenuInflater().inflate(R.menu.menu_default, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.menu_item_new_diary_entry:
                intent = new Intent(this, NewDiaryEntryActivity.class);
                startActivityForResult(intent, REQUEST_NEW_DIARY_ENTRY);
                return true;

            case R.id.menu_item_profile_settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivityForResult(intent, REQUEST_SETTINGS);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            /**
             * If we come from NEW_DIARY_ENTRY or DIARY_ENTRY_DETAIL we are going to update the UI
             */
            case REQUEST_NEW_DIARY_ENTRY:
            case ActiveDiaryEntryAdapter.REQUEST_ACTIVITY_DIARY_ENTRY_DETAIL:
                if (resultCode == RESULT_OK) {
                    viewPager.setCurrentItem(PAGE_CURRENT_ACTIVE_ENTRIES);
                    ((NavigationCurrentActiveEntriesFragment) pagerAdapter.getRegisteredFragment(PAGE_CURRENT_ACTIVE_ENTRIES))
                            .getAdapter().updataListData(DatabaseManager.getInstance(this).getAllCurrentActiveDiaryEntries());
                }
                break;

            /**
             * If we come from ADD_FROM_GALLERY from an card we have to save
             * the cropped image to the database and handle the multiple images
             * which could have been selected
             */
            case UCrop.REQUEST_CROP:
                DiaryEntry diaryEntryLastAdd = ((NavigationCurrentActiveEntriesFragment) pagerAdapter.getRegisteredFragment(PAGE_CURRENT_ACTIVE_ENTRIES)).getDiaryEntryLastAdd();
                AddPictureResultHandler.onImageCroppedResult(this, diaryEntryLastAdd, resultCode, data);
                ((NavigationCurrentActiveEntriesFragment) pagerAdapter.getRegisteredFragment(PAGE_CURRENT_ACTIVE_ENTRIES))
                        .getAdapter().updataListData(DatabaseManager.getInstance(this).getAllCurrentActiveDiaryEntries());
                break;

            case REQUEST_SETTINGS:
                pagerAdapter.getRegisteredFragment(PAGE_PROFILE).updateData();
                break;
        }
    }

    /**
     * This method is setting up the viewpager,
     * it is assigning an adapter and assigning the smart tab layout
     */
    private void setupViewpager() {
        pagerAdapter = new NavigationPagerAdapter(getSupportFragmentManager(), NUM_NAVIGATION_PAGES);
        viewPager.setAdapter(pagerAdapter);
        setupSmartTabs(viewPagerTabLayout, this);
        viewPagerTabLayout.setViewPager(viewPager);
    }

    /**
     * This method is setting up the smart-tabs
     *
     * @param layout   The layout which you want to setup
     * @param activity The activity in which the smart tab layout is placed
     */
    private void setupSmartTabs(SmartTabLayout layout, final Activity activity) {
        final LayoutInflater inflater = LayoutInflater.from(layout.getContext());

        layout.setCustomTabView(new SmartTabLayout.TabProvider() {
            /**
             * For every tab we are creating a layout and returning it
             * @param position The current position
             * @return A view which represents the layout of the tab
             */
            @Override
            public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
                RelativeLayout relativeLayout = (RelativeLayout) inflater.inflate(R.layout.activity_navigation_tab, container, false);

                viewPagerTabs[position] = (ImageView) relativeLayout.findViewById(R.id.image_navigation_tab);

                // TODO: Return the correct icons
                switch (position) {
                    case 0:
                        viewPagerTabs[position].setImageResource(R.drawable.ic_explore_white_24dp);
                        break;
                    case 1:
                        viewPagerTabs[position].setImageResource(R.drawable.ic_add_circle_white_24dp);
                        break;
                    case 2:
                        viewPagerTabs[position].setImageResource(R.drawable.ic_account_circle_white_24dp);
                        break;
                    default:
                        break;
                }

                return relativeLayout;
            }
        });
    }

    /**
     * This method is getting called as soon as we change the position of the viewpager
     *
     * @param position The position to which we navigate
     */
    @OnPageChange(R.id.viewpager_navigation)
    void pageChanged(int position) {
        // Calls the onCreateOptionsMenu method again, loading the correct menu
        invalidateOptionsMenu();

        /// Create search and disable toolbar for profile
        if (position == PAGE_PROFILE) {
            toolbar.setVisibility(View.GONE);
            if (getCurrentFragment() != null) {
                setSupportActionBar(((NavigationProfileFragment) getCurrentFragment()).getToolbar());

                NavigationProfileFragment fragment = (NavigationProfileFragment) getCurrentFragment();
                fragment.refreshCompletedEntryList();
            }
        } else {
            toolbar.setVisibility(View.VISIBLE);
            setSupportActionBar(toolbar);
        }

        // Filter the icons with the appropriate color-tint
        for (ImageView tab : viewPagerTabs) {
            tab.setColorFilter(ContextCompat.getColor(this, R.color.grey_600));
        }
        viewPagerTabs[position].setColorFilter(ContextCompat.getColor(this, R.color.transparent));
    }

    public NavigationPagerAdapter getNavigationPagerAdapter() {
        return pagerAdapter;
    }

    private NavigationFragment getCurrentFragment() {
        return pagerAdapter.getRegisteredFragment(viewPager.getCurrentItem());
    }

    private NavigationFragment getFragment(int index) {
        return pagerAdapter.getRegisteredFragment(index);
    }
}
