package at.fhhagenberg.photo_diary.ui;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.j256.ormlite.dao.ForeignCollection;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.helper.DiarySettings;
import at.fhhagenberg.photo_diary.helper.ImageManipulator;
import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.model.Image;
import at.fhhagenberg.photo_diary.sql.DatabaseManager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class GalleryDetailActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_GALLERY_POSITION = "Position";
    public static final String EXTRA_ENTRY_ID = "Entry";
    public static final int POSITION_LAST = -1;

    private int entryID;
    private int position;
    private Image[] images;
    private Drawable[] drawables;
    private int quality;
    private boolean initialized = false;
    protected DiaryEntry entry;


    @BindView(R.id.active_diary_gallery_next)
    protected ImageButton next;

    @BindView(R.id.active_diary_gallery_previous)
    protected ImageButton previous;

    @BindView(R.id.active_diary_gallery_image_switcher)
    protected ImageSwitcher imageSwitcher;

    @BindView(R.id.txt_active_gallery_name)
    protected TextView name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_detail);
        ButterKnife.bind(this);

        position = getIntent().getIntExtra(EXTRA_GALLERY_POSITION, -2);
        entryID = getIntent().getIntExtra(EXTRA_ENTRY_ID, -2);
        quality = DiarySettings.getCompletedDiaryExportQuality(this);

        previous.setOnClickListener(this);
        next.setOnClickListener(this);

        if (position < -1 || entryID < 0) {
            Toast.makeText(this, "There are no images to be displayed. This should not have happened.", Toast.LENGTH_SHORT).show();
            finish();
        }

        //Get images from database and create drawables
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                entry = DatabaseManager.getInstance(getApplicationContext()).getDiaryEntry(entryID);
                ForeignCollection<Image> temp = entry.getImages();
                images = new Image[temp.size()];
                temp.toArray(images);

                drawables = imagesToDrawable(images);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (position == -1) {
                    position = (drawables.length - 1);
                }
                imageSwitcher.setImageDrawable(drawables[position]);
                name.setText(entry.getName());
                initialized = true;
                Log.i("GalleryDetail", "Initial Position: " + position);
                Log.i("GalleryDetail", "Number of images: " + images.length);
                super.onPostExecute(aVoid);
            }
        }.execute();

        //Set image view factory to imageswitcher
        imageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView view = new ImageView(getApplicationContext());
                view.setAdjustViewBounds(true);
                view.setScaleType(ImageView.ScaleType.CENTER_CROP);
                view.setLayoutParams(new ImageSwitcher.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT));
                return view;
            }
        });

    }

    /**
     * Creates a array of Drawables (BitmapDrawable) from a array of Images
     *
     * @param images The array of Images that has to be converted.
     * @return A array of Drawables that are created from the Images.
     */
    private Drawable[] imagesToDrawable(Image[] images) {
        Drawable[] drawables = new Drawable[images.length];

        for (int i = 0; i < images.length; i++) {
            drawables[i] = new BitmapDrawable(this.getResources(), ImageManipulator.getThumbnailFromFile(images[i].getFilePath(), quality, quality));
            Log.i("CompletedDiary", "drawables: " + drawables[i]);
        }

        return drawables;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.active_diary_gallery_next: {
                if (initialized) {
                    position++;
                    if (position >= drawables.length) {
                        position = 0;
                    }
                    imageSwitcher.setInAnimation(this, R.anim.gallery_slide_in_right);
                    imageSwitcher.setOutAnimation(this, R.anim.gallery_slide_out_left);
                    imageSwitcher.setImageDrawable(drawables[position]);
                }
                break;
            }
            case R.id.active_diary_gallery_previous: {
                if (initialized) {
                    position--;
                    if (position < 0) {
                        position = (drawables.length - 1);
                    }
                    imageSwitcher.setInAnimation(this, R.anim.gallery_slide_in_left);
                    imageSwitcher.setOutAnimation(this, R.anim.gallery_slide_out_right);
                    imageSwitcher.setImageDrawable(drawables[position]);
                }
                break;
            }
            default: {
                Log.i("GalleryDetailActivity", "This id is unknown");
            }
        }
    }
}
