package at.fhhagenberg.photo_diary.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.j256.ormlite.dao.ForeignCollection;
import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.define.Define;
import com.yalantis.ucrop.UCrop;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.adapter.ActiveDiaryEntryAdapter;
import at.fhhagenberg.photo_diary.adapter.ActiveDiaryGalleryAdapter;
import at.fhhagenberg.photo_diary.helper.AddPictureResultHandler;
import at.fhhagenberg.photo_diary.helper.CameraHelper;
import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.model.Image;
import at.fhhagenberg.photo_diary.service.DiaryEntryService;
import at.fhhagenberg.photo_diary.sql.DatabaseManager;
import at.fhhagenberg.photo_diary.view.DialogCreator;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Activity that displays the details of a active diary entry.
 */
public class DiaryEntryDetailActivity extends AppCompatActivity {

    private final int REQUEST_PERMISSION_CAMERA = 100;
    private final int REQUEST_ACTIVITY_CAMERA = 101;

    @BindView(R.id.activity_active_diary_entry_detail_toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.active_diary_entry_detail_gallery)
    protected RecyclerView recyclerView;

    private DiaryEntry diaryEntry;
    private Image[] images;
    private Drawable[] drawables;

    DialogInterface.OnClickListener deleteDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    DiaryEntryService.getInstance().deleteDiaryEntry(getApplicationContext(), diaryEntry);
                    setResult(RESULT_OK);
                    finish();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_diary_entry_detail);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layoutManager);

        diaryEntry = DatabaseManager.getInstance(this).getDiaryEntry(getIntent().getIntExtra(ActiveDiaryEntryAdapter.EXTRA_SELECTED_DIARY_ENTRY, -1));
        if (diaryEntry == null) {
            Log.e("DiaryEntryDetail", "passed entry was null");
            finish();
        }

        //Get an array of images in the diary entry
        updatePhotos();

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(diaryEntry.getName());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //textViewFrom.setText(TimeHelper.dateTimeToReadableDate(this, diaryEntry.getFrom()));
        //textViewTo.setText(TimeHelper.dateTimeToReadableDate(this, diaryEntry.getTo()));
        //updateNumberOfPhotos();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_diary_entry_detail, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (diaryEntry != null) {
            if (diaryEntry.isManualCompletionActive()) {
                menu.findItem(R.id.menu_item_diary_entry_detail_complete).setVisible(true);
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_diary_entry_detail_delete:
                DialogCreator.showDialogOKCancel(this, getResources().getString(R.string.dialog_delete_entry_question),
                        getResources().getString(R.string.dialog_delete_entry_yes),
                        getResources().getString(R.string.dialog_delete_entry_no),
                        deleteDialogClickListener, null);
                return true;

            case R.id.menu_item_diary_entry_detail_complete:
                if (diaryEntry.getImages().size() > 0) {
                    DiaryEntryService.getInstance().completeDiaryEntry(this, diaryEntry);
                    setResult(RESULT_OK);
                    finish();
                    return true;
                } else {
                    Toast.makeText(this, "Entries without images cannot be completed", Toast.LENGTH_SHORT).show();
                    return false;
                }

            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Define.ALBUM_REQUEST_CODE) {
            AddPictureResultHandler.onAddFromGalleryResult(this, diaryEntry, resultCode, data);
        } else if (requestCode == UCrop.REQUEST_CROP) {
            AddPictureResultHandler.onImageCroppedResult(this, diaryEntry, resultCode, data);
        }

        if (resultCode == RESULT_OK) {
            // set result so the number of photos gets also changed on the navigation-activity
            setResult(RESULT_OK);
            updatePhotos();
        }
    }

    @OnClick(R.id.btn_diary_entry_detail_add_from_camera)
    protected void onAddFromCameraClick() {
        if (!CameraHelper.isCameraHardwareAvailable(this)) {
            Toast.makeText(this, getResources().getString(R.string.error_no_camera_available), Toast.LENGTH_SHORT).show();
            return;
        }

        // If the user has not already granted the permission for camera and writing to external storage
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // If the users has chosen the "never ask again" option
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                DialogCreator.showDialogOKCancel(this, getResources().getString(R.string.request_camera_permissions), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(DiaryEntryDetailActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA);
                    }
                }, null);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CAMERA);
            }
        } else {
            startCameraActivity();
        }
    }

    /**
     * Opens an activity which lists all available pictures on the device.
     * After selecting the desired images the onActivityResult is called, and the
     * desired info can be requested from the "Define.ALBUM_REQUEST_CODE" request
     * Permissions are handled by FishBun
     */
    @OnClick(R.id.btn_diary_entry_detail_add_from_gallery)
    protected void onAddFromGalleryClick() {
        FishBun.with(DiaryEntryDetailActivity.this)
                .setActionBarColor(ContextCompat.getColor(this, R.color.colorPrimary), ContextCompat.getColor(this, R.color.colorPrimaryDark))
                .setPickerCount(99)
                .setButtonInAlbumActivity(false)
                .startAlbum();
    }

    /**
     * Refreshes the list of images that is displayed in the gallery
     */
    private void updatePhotos() {
        ForeignCollection<Image> imageData = diaryEntry.getImages();
        images = new Image[imageData.size()];
        imageData.toArray(images);

        ActiveDiaryGalleryAdapter adapter = new ActiveDiaryGalleryAdapter(images, this, diaryEntry.getExternalId());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CAMERA: {
                if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    // Permission was granted!
                    startCameraActivity();
                } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // Camera permission denied
                    Toast.makeText(this, getResources().getString(R.string.error_no_camera_permission), Toast.LENGTH_SHORT).show();
                } else if (grantResults.length > 1 && grantResults[1] == PackageManager.PERMISSION_DENIED) {
                    // External storage permission denied
                    Toast.makeText(this, getResources().getString(R.string.error_no_write_permission), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.error_no_camera_permissions), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    /**
     * Starts the camera activity
     */
    private void startCameraActivity() {
        Intent intent = new Intent(this, CameraActivity.class);
        intent.putExtra(ActiveDiaryEntryAdapter.EXTRA_SELECTED_DIARY_ENTRY, diaryEntry.getExternalId());
        startActivityForResult(intent, REQUEST_ACTIVITY_CAMERA);
    }
}
