package at.fhhagenberg.photo_diary.ui.preference;

import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;

import com.mikepenz.aboutlibraries.LibsBuilder;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.helper.DiarySettings;

public class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener {

    private EditTextPreference edtFirstName;
    private EditTextPreference edtLastName;
    private SwitchPreference switchPreference;
    private Preference preference;
    private ListPreference autoplayPreference;
    private ListPreference gifSpeedPreference;
    private ListPreference gifQualityPreference;
    private SwitchPreference highResolutionSaving;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_screen);
        edtFirstName = (EditTextPreference) findPreference("first_name");
        edtLastName = (EditTextPreference) findPreference("last_name");
        switchPreference = (SwitchPreference) findPreference("notification_enabled");
        preference = findPreference("open_source_libraries");
        autoplayPreference = (ListPreference) findPreference("completed_diary_autoplay");
        gifSpeedPreference = (ListPreference) findPreference("completed_diary_export_speed");
        gifQualityPreference = (ListPreference) findPreference("completed_diary_export_quality");
        highResolutionSaving = (SwitchPreference) findPreference("camera_high_resolution_saving_enabled");

        edtFirstName.setOnPreferenceChangeListener(this);
        edtLastName.setOnPreferenceChangeListener(this);
        switchPreference.setOnPreferenceChangeListener(this);
        preference.setOnPreferenceClickListener(this);
        autoplayPreference.setOnPreferenceChangeListener(this);
        gifSpeedPreference.setOnPreferenceChangeListener(this);
        gifQualityPreference.setOnPreferenceChangeListener(this);
        highResolutionSaving.setOnPreferenceChangeListener(this);

        edtFirstName.setText(DiarySettings.getPersonalizationFirstName(getActivity()));
        edtLastName.setText(DiarySettings.getPersonalizationLastName(getActivity()));
        switchPreference.setDefaultValue(DiarySettings.isNotificationEnabled(getActivity()));
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String key = preference.getKey();

        if ("first_name".equals(key)) {
            DiarySettings.setPersonalizationFirstName(getActivity(), String.valueOf(newValue));
        } else if ("last_name".equals(key)) {
            DiarySettings.setPersonalizationLastName(getActivity(), String.valueOf(newValue));
        } else if ("notification_enabled".equals(key)) {
            DiarySettings.setNotificationEnabled(getActivity(), (boolean) newValue);
        } else if ("completed_diary_autoplay".equals(key)) {
            DiarySettings.setCompletedDiaryAutoplay(getActivity(), Integer.valueOf(String.valueOf(newValue)));
        } else if ("completed_diary_export_speed".equals(key)) {
            DiarySettings.setCompletedDiaryGifSpeed(getActivity(), Integer.valueOf(String.valueOf(newValue)));
        } else if ("completed_diary_export_quality".equals(key)) {
            DiarySettings.setCompletedDiaryExportQuality(getActivity(), Integer.valueOf(String.valueOf(newValue)));
        } else if ("camera_high_resolution_saving_enabled".equals(key)) {
            DiarySettings.setHighResolutionSavingEnabled(getActivity(), (boolean) newValue);
        }

        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if ("open_source_libraries".equals(preference.getKey())) {
            new LibsBuilder()
                    .withFields(R.string.class.getFields())
                    .withActivityTitle(getString(R.string.settings_open_source_libraries))
                    .start(getActivity());
        }
        return true;
    }
}
