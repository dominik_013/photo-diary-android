package at.fhhagenberg.photo_diary.ui.onboarding;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import at.fhhagenberg.photo_diary.R;

public class OnboardingWelcomeFragment extends OnboardingFragment {

    public OnboardingWelcomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_onboarding_welcome, container, false);
    }
}
