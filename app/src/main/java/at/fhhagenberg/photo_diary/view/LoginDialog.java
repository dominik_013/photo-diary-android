package at.fhhagenberg.photo_diary.view;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import at.fhhagenberg.photo_diary.R;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginDialog extends DialogFragment {
    // Empty constructor required for DialogFragment
    public LoginDialog() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_dialog, container);
        ButterKnife.bind(this, view);

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getDialog().setTitle(getResources().getString(R.string.text_login_dialog_title));
        setStyle(DialogFragment.STYLE_NORMAL, R.style.LoginDialog);

        return view;
    }

    @OnClick(R.id.btn_login_dialog_login)
    protected void onClickLogin() {
        // Todo: login with restclient
    }

    @OnClick(R.id.btn_login_dialog_cancel)
    protected void onClickCancel() {
        getDialog().dismiss();
    }
}
