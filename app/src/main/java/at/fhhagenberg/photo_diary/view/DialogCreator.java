package at.fhhagenberg.photo_diary.view;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import at.fhhagenberg.photo_diary.R;


public class DialogCreator {
    /**
     * Shows a rationale dialog, if the user has used the "never ask again" checkbox for the permission
     *
     * @param msg                   The message that is going to be displayed on the dialog
     * @param onClickOKListener     The listener which is called when the user presses the "OK" button
     * @param onClickCancelListener The listener which is called when the user presses the "Cancel" button
     */
    public static void showDialogOKCancel(final Context context, String msg, DialogInterface.OnClickListener onClickOKListener, DialogInterface.OnClickListener onClickCancelListener) {
        showDialogOKCancel(context, msg, context.getResources().getString(R.string.action_ok), context.getResources().getString(R.string.action_cancel), onClickOKListener, onClickCancelListener);
    }

    public static void showDialogOKCancel(final Context context, String msg, String okText, String cancelText,
                                          DialogInterface.OnClickListener onClickOKListener, DialogInterface.OnClickListener onClickCancelListener) {
        new AlertDialog.Builder(context)
                .setMessage(msg)
                .setPositiveButton(okText, onClickOKListener)
                .setNegativeButton(cancelText, onClickCancelListener)
                .create()
                .show();
    }
}
