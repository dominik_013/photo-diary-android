package at.fhhagenberg.photo_diary.view;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

import at.fhhagenberg.photo_diary.helper.CameraHelper;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder holder;
    private Camera camera;

    private static final double ASPECT_RATIO = 1.0 / 1.0;
    private static final double SECONDARY_ASPECT_RATIO = 4.0 / 3.0;

    public CameraPreview(Context context, Camera camera) {
        super(context);
        this.camera = camera;
        holder = getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    /**
     * Measure the view and its content to determine the measured width and the measured height.
     * Since we want a square image (and preview) we have to set width and height to the same value
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        //Log.i("CameraPreview", "Measured width: " + width + "Measured Height: " + height);

        //width and height of MeasureSpec are height and width of Camera.Size if phone is upright
        if (width < height) {
            Camera.Size size = CameraHelper.determineBestPreviewSize(camera.getParameters(), height, width);
            float aspectRatio = (float) size.width / size.height;
            setMeasuredDimension(width, (int) (width * aspectRatio));
        } else {
            Camera.Size size = CameraHelper.determineBestPreviewSize(camera.getParameters(), width, height);
            float aspectRatio = (float) size.width / size.height;
            setMeasuredDimension((int) (height * aspectRatio), height);
        }

    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        } catch (IOException e) {
            Log.d("Camera Preview", "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        if (this.holder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // Stop the preview before making changes (e.g. rotation)
        try {
            camera.stopPreview();
        } catch (Exception ex) {
            Log.e("Camera Preview", "Error stoping camera: " + ex.getMessage());
        }

        // Make changes
        try {
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                camera.setDisplayOrientation(90);
            } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                camera.setDisplayOrientation(0);
            }
        } catch (Exception ex) {
            Log.e("Camera Preview", "Error rotating camera: " + ex.getMessage());
        }

        // Start the preview here with the new settings
        try {
            camera.setPreviewDisplay(this.holder);
            camera.startPreview();

        } catch (Exception ex) {
            Log.d("Camera Preview", "Error starting camera preview: " + ex.getMessage());
        }
    }
}