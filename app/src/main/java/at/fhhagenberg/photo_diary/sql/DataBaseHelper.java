package at.fhhagenberg.photo_diary.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.model.Image;

/**
 * Class used by the DatabaseManager to manage the local database.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "viary.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = "DatabaseHelper";

    private Dao diaryEntryDao;
    private Dao imageDao;

    /**
     * A single instance of DatabaseHelper.
     */
    private static DatabaseHelper instance = null;

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION); //load the config file in res/raw to speed up dao creation

        diaryEntryDao = null;
        imageDao = null;
    }

    /**
     * Returns an instance of DatabaseHelper.
     *
     * @param context The context to create a DatabaseHelper with.
     * @return A DatabaseHelper instance.
     */
    public static DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, DiaryEntry.class);
            TableUtils.createTable(connectionSource, Image.class);
        } catch (SQLException e) {
            Log.e(TAG, "Can't create database.");
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        //RESERVED FOR UPDATES TO FUTURE DATABASE VERSIONS
    }

    public void clearTable(Class clazz) {
        try {
            TableUtils.clearTable(connectionSource, clazz);
        } catch (SQLException e) {
            Log.e(TAG, "Can't drop databases.");
            throw new RuntimeException(e);
        }
    }

    public void clearDataTables() {
        try {
            TableUtils.clearTable(connectionSource, DiaryEntry.class);
            TableUtils.clearTable(connectionSource, Image.class);
        } catch (SQLException e) {
            Log.e(TAG, "Can't drop databases.");
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public Dao<DiaryEntry, Integer> getDiaryEntryDao() throws SQLException {
        if (diaryEntryDao == null) {
            diaryEntryDao = getDao(DiaryEntry.class);
        }

        return diaryEntryDao;
    }

    public Dao<Image, Integer> getImageDao() throws SQLException {
        if (imageDao == null) {
            imageDao = getDao(Image.class);
        }

        return imageDao;
    }

    public int getDatabaseVersion() {
        return DATABASE_VERSION;
    }
}