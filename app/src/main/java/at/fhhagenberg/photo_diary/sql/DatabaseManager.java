package at.fhhagenberg.photo_diary.sql;

import android.content.Context;

import com.j256.ormlite.stmt.Where;

import org.joda.time.DateTime;

import java.sql.SQLException;
import java.util.List;

import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.model.Image;
import at.fhhagenberg.photo_diary.model.ModelBaseClass;

/**
 * Class that manages local database access.
 */
public class DatabaseManager {
    private static DatabaseManager instance;
    private final DatabaseHelper helper;

    private static final String DIARY_ENTRY_NAME = "name";
    private static final String DIARY_ENTRY_FROM = "from";
    private static final String DIARY_ENTRY_TO = "to";
    private static final String DIARY_ENTRY_IS_MANUAL_COMPLETION_ACTIVE = "isManualCompletionActive";
    private static final String DIARY_ENTRY_IS_MANUAL_COMPLETION_FINISHED = "isManualCompletionFinished";
    private static final String DIARY_ENTRY_IS_NOTIFICATION_ENABLED = "isNotificationEnabled";

    private static final String DIARY_ENTRY_ID = "diary_entry_id";

    private DatabaseManager(Context context) {
        this.helper = DatabaseHelper.getInstance(context);
    }

    public static synchronized DatabaseManager getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseManager(context);
        }
        return instance;
    }

    /* Create or update */

    /**
     * Creates or updates the passed entry
     *
     * @param entry The diary-entry which has to be created/updated
     */
    public synchronized void createOrUpdateDiaryEntry(DiaryEntry entry) {
        try {
            helper.getDiaryEntryDao().createOrUpdate(entry);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates the passed image in the database
     *
     * @param image The image which has to be created
     */
    public synchronized void createImage(Image image) {
        try {
            helper.getImageDao().create(image);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /* Get single elements */

    /**
     * Gets a specific diary entry
     *
     * @param id The external id from the diary-entry we want to get from the database
     * @return The queried diary-entry
     */
    public synchronized DiaryEntry getDiaryEntry(int id) {
        try {
            return helper.getDiaryEntryDao().queryForId(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /* Get list of elements */

    /**
     * Returns all available Diary entries
     *
     * @return A list of all available diary entries
     */
    public synchronized List<DiaryEntry> getAllDiaryEntries() {
        try {
            return helper.getDiaryEntryDao().queryBuilder().orderBy(ModelBaseClass.ID, true).query();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Gets the current active diary entries, so the one where "from date" <= "DateTime.now()" <= "to date"
     * If we saved the diary-entry with the property isManualCompletionActive, it is also returned
     *
     * @return A list of the current active entries
     */
    public synchronized List<DiaryEntry> getAllCurrentActiveDiaryEntries() {
        try {
            return helper.getDiaryEntryDao().queryBuilder().orderBy(ModelBaseClass.ID, true)
                    .where().ge(DIARY_ENTRY_TO, DateTime.now())
                    .and().le(DIARY_ENTRY_FROM, DateTime.now())
                    .or().eq(DIARY_ENTRY_IS_MANUAL_COMPLETION_ACTIVE, true).and().eq(DIARY_ENTRY_IS_MANUAL_COMPLETION_FINISHED, false)
                    .query();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Gets the current active diary entries which have notifications enabled,
     * so the one where "from date" <= "DateTime.now()" <= "to date"
     * If we saved the diary-entry with the property isManualCompletionActive, it is also returned
     *
     * @return A list of the current active entries where notifications are enabled
     */
    public synchronized List<DiaryEntry> getAllCurrentActiveDiaryEntriesWithNotifications() {
        try {
            return helper.getDiaryEntryDao().queryBuilder().orderBy(ModelBaseClass.ID, true)
                    .where().ge(DIARY_ENTRY_TO, DateTime.now())
                    .and().le(DIARY_ENTRY_FROM, DateTime.now())
                    .or().eq(DIARY_ENTRY_IS_MANUAL_COMPLETION_ACTIVE, true).and().eq(DIARY_ENTRY_IS_MANUAL_COMPLETION_FINISHED, false)
                    .and().eq(DIARY_ENTRY_IS_NOTIFICATION_ENABLED, true)
                    .query();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Gets all finished diary entries
     * (where the "to date" < "DateTime.now()" or where manual completion is finished)
     *
     * @return A list of all finished diary entries
     */
    public synchronized List<DiaryEntry> getAllFinishedDiaryEntries() {
        try {
            return helper.getDiaryEntryDao().queryBuilder().orderBy(ModelBaseClass.ID, true)
                    .where().lt(DIARY_ENTRY_TO, DateTime.now())
                    .or()
                    .eq(DIARY_ENTRY_IS_MANUAL_COMPLETION_ACTIVE, true).and().eq(DIARY_ENTRY_IS_MANUAL_COMPLETION_FINISHED, true)
                    .query();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Gets all diary entries that are completed and whose names contain the given pattern
     * (Pattern must be in form "%pattern%" in order to work correctly.
     *
     * @param pattern The pattern that is looked for
     * @return A list of all diary entries that match the pattern
     */
    public synchronized List<DiaryEntry> getAllMatchingDiaryEntries(String pattern) {
        try {
            Where<DiaryEntry, Integer> where = helper.getDiaryEntryDao().queryBuilder().where();
            return where.and(where.like(DIARY_ENTRY_NAME, pattern),
                    where.or(where.lt(DIARY_ENTRY_TO, DateTime.now()),
                            where.and(where.eq(DIARY_ENTRY_IS_MANUAL_COMPLETION_ACTIVE, true),
                                    where.eq(DIARY_ENTRY_IS_MANUAL_COMPLETION_FINISHED, true)))).query();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    //TODO: Think of a place to use it in the application where it is only called when needed (maybe Broadcast Receiver?)

    /**
     * Deletes all finished diary entries that have no images stored inside them and were not manually completed.
     */
    public synchronized void deleteEmptyFinishedEntries() {
        try {
            //Get all finished diary entries that were not manually completed
            List<DiaryEntry> entries = helper.getDiaryEntryDao().queryBuilder().orderBy(ModelBaseClass.ID, true)
                    .where().lt(DIARY_ENTRY_TO, DateTime.now()).query();

            if (entries != null) {
                for (DiaryEntry entry : entries) {
                    if (entry.getImages().size() < 1) {
                        List<Image> imageList = helper.getImageDao().queryBuilder().where().eq(DIARY_ENTRY_ID, entry.getExternalId()).query();
                        helper.getImageDao().delete(imageList);
                        helper.getDiaryEntryDao().delete(entry);
                    }
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /* Delete */

    /**
     * Deletes a specific diary-entry, all image-data associated with the diary entry get also deleted from the db
     *
     * @param entry The entry to be deleted
     */
    public synchronized void deleteDiaryEntry(DiaryEntry entry) {
        try {
            List<Image> imageList = helper.getImageDao().queryBuilder().where().eq(DIARY_ENTRY_ID, entry.getExternalId()).query();
            helper.getImageDao().delete(imageList);
            helper.getDiaryEntryDao().delete(entry);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
