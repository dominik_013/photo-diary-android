package at.fhhagenberg.photo_diary.sql;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.File;

import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.model.Image;

/**
 * Utility for creating a config file for the ORMlite entities.
 * <p/>
 * Every time something in the entities changes, this class must be locally run.
 * It regenerates the ormlite_config.txt file, located at res/raw/.
 * <p/>
 * Doc:
 * http://ormlite.com/javadoc/ormlite-core/doc-files/ormlite_4.html#Config-Optimization
 */

public class DatabaseConfigUtil extends OrmLiteConfigUtil {

    /* All model classes which will be used by the ORM */
    private static final Class<?>[] classes = new Class[]{DiaryEntry.class, Image.class};

    public static void main(String[] args) throws Exception {

        String currDirectory = "user.dir";
        String configPath = "/app/src/main/res/raw/ormlite_config.txt";

        /**
         * Gets the project root directory
         */
        String projectRoot = System.getProperty(currDirectory);

        /**
         * Full configuration path includes the project root path, and the location
         * of the ormlite_config.txt file appended to it
         */
        String fullConfigPath = projectRoot + configPath;

        File configFile = new File(fullConfigPath);

        /**
         * In the a scenario where we run this program several times, it will recreate the
         * configuration file each time with the updated configurations.
         */
        if (configFile.exists()) {
            configFile.delete();
            configFile = new File(fullConfigPath);
        }

        /**
         * writeConfigFile is a util method used to write the necessary configurations
         * to the ormlite_config.txt file.
         */
        writeConfigFile(configFile, classes);
    }
}
