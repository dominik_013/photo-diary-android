package at.fhhagenberg.photo_diary.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.preference.PreferenceManager;

import java.util.List;

@SuppressWarnings("deprecation")
public class CameraHelper {

    private static final String CAMERA_HELPER_KEY = "CameraHelper";
    private static final String FLASH_MODE = CAMERA_HELPER_KEY + "flashMode";
    private static final String CAMERA_ID = CAMERA_HELPER_KEY + "facingDirection";

    /**
     * Checks if the device has got a camera
     *
     * @return True if there is a hardware camera available
     */
    public static boolean isCameraHardwareAvailable(final Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    /**
     * Determines the best PREVIEW Size available (checks if a 1:1 aspect ratio is possible, if not
     * we are going to take the best 4:3 aspect image)
     *
     * @param parameters The parameters of the used camera
     * @return The best size of the preview (1:1 if possible, else 4:3)
     */
    public static Camera.Size determineBestPreviewSize(Camera.Parameters parameters, int screenWidth, int screenHeight) {
        List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
        //Get the size of the shorter part of the display
        int maxSize = 0;
        //float aspectRatio = 0.0f;
        Camera.Size returnSize = sizes.get(0);

        if (screenWidth < screenHeight) {
            maxSize = screenWidth;
        } else {
            maxSize = screenHeight;
        }

        for (Camera.Size size : sizes) {
            if (size.height == returnSize.height && size.height <= maxSize) {
                if ((size.height * size.width) < (returnSize.height * returnSize.width)) {
                    returnSize = size;
                }
            } else if (size.height > returnSize.height && size.height <= maxSize) {
                returnSize = size;
            } else if (returnSize.height > maxSize && size.height < returnSize.height) {
                returnSize = size;
            }
        }

        //Log.i("CameraPreview", "Best preview size is: " + returnSize.width + " x " + returnSize.height);
        return returnSize;

    }

    /**
     * Determines the best PICTURE Size available (checks if a 1:1 aspect ratio is possible, if not
     * we are going to take the best 4:3 aspect image)
     *
     * @param parameters The parameters of the used camera
     * @return The best size of the picture (1:1 if possible, else 4:3)
     */
    public static Camera.Size determineBestPictureSize(Camera.Parameters parameters) {
        List<Camera.Size> sizes = parameters.getSupportedPictureSizes();

        Camera.Size returnSize = sizes.get(0);

        for (Camera.Size size : sizes) {
            if (size.height > returnSize.height) {
                returnSize = size;
            }
        }

        return returnSize;
    }

    /**
     * Returns the front camera id if a front camera is available
     * Else it returns the id of the back camera
     *
     * @param context Context
     * @return The id of the front facing camera if available, else the id of the back facing camera
     */
    public static int getFrontCameraIDIfAvailable(final Context context) {
        PackageManager pm = context.getPackageManager();
        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
            return Camera.CameraInfo.CAMERA_FACING_FRONT;
        }

        return getBackCameraID();
    }

    /**
     * Returns the back-facing camera
     *
     * @return The id of the back facing camera
     */
    public static int getBackCameraID() {
        return Camera.CameraInfo.CAMERA_FACING_BACK;
    }

    /**
     * @return true if the device has got the ability to use flash
     */
    public static boolean isFlashAvailable(final Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    /**
     * @return true if the device has got the ability to use the continuous-focus-mode
     */
    public static boolean isContinuousFocusSupported(final Camera.Parameters parameters) {
        return parameters.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
    }

    /**
     * Persisting the options we set in our camera-activity
     */
    public static void storeFlashMode(final Context context, String flashMode) {
        getSharedPreferences(context).edit().putString(FLASH_MODE, flashMode).apply();
    }

    /**
     * Gets the currently set flash mode from shared preferences
     *
     * @param context The context this method is called from
     * @return Returns the stored flash mode as String
     */
    public static String obtainFlashMode(final Context context) {
        return getSharedPreferences(context).getString(FLASH_MODE, Camera.Parameters.FLASH_MODE_OFF);
    }

    /**
     * Stores the camera ID to the shared preferences
     *
     * @param context  The context this method is called from
     * @param cameraId The cameraID that should be stored in shared preferences
     */
    public static void storeCameraId(final Context context, int cameraId) {
        getSharedPreferences(context).edit().putInt(CAMERA_ID, cameraId).apply();
    }

    /**
     * Gets the currently set camera id from shared preferences
     *
     * @param context The context this method is called from
     * @return Returns the stored cameraID as int.
     */
    public static int obtainCameraId(final Context context) {
        return getSharedPreferences(context).getInt(CAMERA_ID, getBackCameraID());
    }

    /**
     * Gets the default shared preferences.
     *
     * @param context The context this method is called from
     * @return Returns the default shared preferences as SharedPreference object.
     */
    private static SharedPreferences getSharedPreferences(final Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
