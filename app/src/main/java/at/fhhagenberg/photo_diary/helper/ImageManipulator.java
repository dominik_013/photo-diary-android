package at.fhhagenberg.photo_diary.helper;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Class that provides various static methods to create, manipulate and store bitmaps.
 */
public class ImageManipulator {

    final static String DIRECTORY_OUTPUT_HIGH_RESOLUTION = "Viary/HD";
    final static String DIRECTORY_OUTPUT_COMPRESSED = "Viary/Compressed";
    final static String DIRECTORY_OUTPUT_PROFILE_PICTURES = "Viary/ProfilePictures";


    /**
     * Gets a file for the taken profile picture
     * The compressed file will be stored in the internal memory
     *
     * @return A file where the image-data can be written to
     */
    public static File getAvailableProfilePictureFile() {
        return getAvailableOutputFile(DIRECTORY_OUTPUT_PROFILE_PICTURES);
    }

    /**
     * Gets a file for the current image with the name "IMG_MM-dd-yyyy_HH-mm-ss.jpg"
     * where MM-dd-yyyy_HH-mm-ss is the current time
     * The compressed file will be stored in the internal memory, so the user has no access to it
     *
     * @return A file where the image-data can be written to
     */
    public static File getAvailableCompressedOutputFile() {
        return getAvailableOutputFile(DIRECTORY_OUTPUT_COMPRESSED);
    }

    /**
     * Gets a file for the current image with the name "IMG_MM-dd-yyyy_HH-mm-ss.jpg"
     * where MM-dd-yyyy_HH-mm-ss is the current time
     * The high resolution image can be optionally saved to the external storage, so the user
     * has access to it
     *
     * @return A file where the image-data can be written to
     */
    public static File getAvailableHiResOutputFile() {
        return getAvailableOutputFile(DIRECTORY_OUTPUT_HIGH_RESOLUTION);
    }

    /**
     * Gets a file for the current image with the name "IMG_MM-dd_yyyy_HH-mm-ss.jpg"
     * where MM-dd-yyyy-HH-mm-ss is the current time.
     * Depending on the parameter directory name the file is stored at /Viary/Compressed
     * or /Viary/HD.
     *
     * @param directoryName The name of the directory the file should be stored in, beginning
     *                      with Viary/
     * @return Returns a new file at the specified directory.
     */
    private static File getAvailableOutputFile(String directoryName) {
        String mediaState = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(mediaState) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(mediaState)) {
            Log.e("Image Manipulator", "external storage not mounted: ");
            return null;
            // TODO: switch to internal storage?
        }

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), directoryName);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Image Manipulator", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        DateTimeFormatter dtf = DateTimeFormat.forPattern("MM-dd-yyyy_HH-mm-ss");
        String timeStamp = dtf.print(DateTime.now());

        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    /**
     * Creates a new asynchronous task which is going to save the image with the right rotation,
     * the saved image will not be compressed with this method
     *
     * @param pictureFile The file where the image-data will be written to
     * @param imageData   The byte array containing the image-data
     * @param rotation    The rotation we have to rotate the image in degree
     */
    public static void saveHighResolutionImage(File pictureFile, byte[] imageData, int rotation) {
        new SaveImageTask(pictureFile, imageData, rotation, false).execute();
    }


    /**
     * Compresses the passed image, rotates it and saves it to the provided File
     *
     * @param pictureFile The file where the image-data will be written to
     * @param imageData   The byte array containing the image-data
     * @param rotation    The rotation we have to rotate the image in degree
     */
    public static void saveCompressedImage(File pictureFile, byte[] imageData, int rotation) {
        //TODO: Compress image
        new SaveImageTask(pictureFile, imageData, rotation, true).execute();
    }

    /**
     * Returns a thumbnail based on the passed size
     *
     * @param reqWidth  The width the thumbnail should have
     * @param reqHeight The height the thumbnail should have
     * @return A bitmap of the scaled image
     */
    public static Bitmap getThumbnailFromByteArray(byte[] imageData, int reqWidth, int reqHeight) {
        return getThumbnailFromByteArray(imageData, reqWidth, reqHeight, 0);
    }

    /**
     * Returns a thumbnail based on the passed size
     *
     * @param reqWidth  The width the thumbnail should have
     * @param reqHeight The height the thumbnail should have
     * @param rotation  The rotation the bitmap should have
     * @return A bitmap of the scaled image
     */
    public static Bitmap getThumbnailFromByteArray(byte[] imageData, int reqWidth, int reqHeight, int rotation) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(imageData, 0, imageData.length, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        Bitmap bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length, options);

        if (rotation == 0) return bitmap;
        else {
            return rotateBitmap(bitmap, rotation);
        }
    }

    /**
     * Returns a thumbnail based on the passed size
     *
     * @param reqWidth  The width the thumbnail should have
     * @param reqHeight The height the thumbnail should have
     * @return A bitmap of the scaled image
     */
    public static Bitmap getThumbnailFromFile(String path, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    /**
     * Calculates the sample size for the options.inSampleSize value in getThumbnailFromFile.
     *
     * @param options   The options the sample size is needed for.
     * @param reqWidth  The required width of the Bitmap.
     * @param reqHeight The required height of the Bitmap.
     * @return Returns the sample size as int value.
     */
    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     * Rotates a Bitmap by a given rotation value.
     *
     * @param source   The Bitmap to be rotated.
     * @param rotation The rotation value it is rotated by.
     * @return Returns the rotated Bitmap as Bitmap.
     */
    private static Bitmap rotateBitmap(Bitmap source, float rotation) {
        Matrix matrix = new Matrix();
        matrix.postRotate(rotation);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    /**
     * Cuts a byte[] in any format to a square format, by cutting out the middle
     * part of it. The result Bitmap has an edge size of the shorter edge size of
     * the original Bitmap.
     *
     * @param imageData The byte[] to be cut to square.
     * @return Returns the cut Bitmap.
     */
    public static Bitmap cutBitmapToSquare(byte[] imageData) {
        Bitmap temp = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);

        //Log.i("ImageManipulator", "Width: " + temp.getWidth() + "Height: " + temp.getHeight());

        //Cut out the middle of the image
        int offset = 0;
        Bitmap cropped;

        if (temp.getWidth() > temp.getHeight()) {
            offset = (temp.getWidth() - temp.getHeight()) / 2;
            cropped = Bitmap.createBitmap(temp, offset, 0, temp.getHeight(), temp.getHeight());
        } else {
            offset = (temp.getHeight() - temp.getWidth());
            cropped = Bitmap.createBitmap(temp, 0, offset, temp.getWidth(), temp.getWidth());
        }

        return cropped;
    }

    /**
     * Cuts a Bitmap in any format to a square format, by cutting out the middle
     * part of it. The result Bitmap has an edge size of the shorter edge size of
     * the original Bitmap.
     *
     * @param imageData The Bitmap to be cut to square.
     * @return Returns the cut Bitmap.
     */
    public static Bitmap cutBitmapToSquare(Bitmap imageData) {

        int offset = 0;
        Bitmap cropped;

        if (imageData.getWidth() > imageData.getHeight()) {
            offset = (imageData.getWidth() - imageData.getHeight()) / 2;
            cropped = Bitmap.createBitmap(imageData, offset, 0, imageData.getHeight(), imageData.getHeight());
        } else {
            offset = (imageData.getHeight() - imageData.getWidth());
            cropped = Bitmap.createBitmap(imageData, 0, offset, imageData.getWidth(), imageData.getWidth());
        }

        return cropped;
    }

    /**
     * Saves a Bitmap to a file asynchronously, depending on the parameters in
     * compressed and resized or uncompressed format.
     */
    private static class SaveImageTask extends AsyncTask<Void, Void, Void> {

        File pictureFile;
        byte[] imageData;
        int rotation;
        boolean compressed;

        SaveImageTask(File pictureFile, byte[] imageData, int rotation, boolean compressed) {
            this.pictureFile = pictureFile;
            this.imageData = imageData;
            this.rotation = rotation;
            this.compressed = compressed;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Log.i("ImageManipulator", "Writing image with: " + ((double) imageData.length / 1000 / 1000) + "MB to " + pictureFile.getPath());
                FileOutputStream fos = new FileOutputStream(pictureFile);

                Bitmap img;

                Bitmap cropped = ImageManipulator.cutBitmapToSquare(imageData);
                if (compressed) {

                    //Resize the image
                    if (cropped.getWidth() >= 1080 && cropped.getHeight() >= 1080) {
                        img = Bitmap.createScaledBitmap(cropped, 1080, 1080, true);
                    } else {
                        img = Bitmap.createScaledBitmap(cropped, cropped.getWidth(), cropped.getHeight(), true);
                    }

                }
                //Save original image without cutting out or resizing anything
                else {
                    img = cropped; //BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
                }

                if (rotation != 0) {
                    img = rotateBitmap(img, rotation);
                }

                ByteArrayOutputStream stream = new ByteArrayOutputStream();

                if (compressed) {
                    img.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                } else {
                    img.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                }

                byte[] byteArray = stream.toByteArray();

                fos.write(byteArray);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException ex) {
                Log.d("CameraActivity", "File not found: " + ex.getMessage());
            } catch (IOException ex) {
                Log.d("CameraActivity", "Error accessing file: " + ex.getMessage());
            } catch (Exception ex) {
                Log.e("Camera Activity", "Error saving image: " + ex.getMessage());
            }

            return null;
        }
    }
}
