package at.fhhagenberg.photo_diary.helper;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.sangcomz.fishbun.define.Define;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.model.Image;
import at.fhhagenberg.photo_diary.sql.DatabaseManager;

import static android.app.Activity.RESULT_OK;

public class AddPictureResultHandler {

    private static ArrayList<String> imagesToCropList = null;

    /**
     * This method gets called when images were selected via the add-from-gallery method
     *
     * @param diaryEntry The diary-entry where the selected images should be saved to
     * @param resultCode ResultCode from onActivityResult
     * @param data       Data intent from onActivityResult
     */
    public static void onAddFromGalleryResult(final Activity activity, DiaryEntry diaryEntry, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            // Get the paths of all the selected images
            imagesToCropList = data.getStringArrayListExtra(Define.INTENT_PATH);

            startCroppingActivity(activity);
        }
    }

    /**
     * Handles what should happen after an image was cropped
     * If the cropping was successful and not canceled we are going to save the
     * cropped image to the database, if it was not successful it just gets deleted from the list
     * If there are images left in the 'imagesToCropList' the next crop-activity is started
     *
     * @param activity   Activity
     * @param diaryEntry The diary entry where we want to save the images to
     * @param resultCode The result code of the crop
     * @param data       The data containing the output file
     */
    public static void onImageCroppedResult(final Activity activity, DiaryEntry diaryEntry, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            // Image was cropped successfully we can now save it to the db
            final Uri resultUri = UCrop.getOutput(data);

            // Something that should not occur happened!
            if (resultUri == null) {
                throw new NullPointerException();
            }

            //Compress image stored at result uri and write it to the same file again
            //We do not have to check if path exists because we get back the file uri
            //where we can take our path from

            byte[] imageData;
            try {
                Bitmap bmp = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), resultUri);
                Log.i("AddPictureResult", "Size: " + bmp.getWidth() + " x " + bmp.getHeight());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                imageData = stream.toByteArray();

                File file = new File(resultUri.getPath());
                FileOutputStream fos = new FileOutputStream(file);
                fos.write(imageData);
                fos.flush();
                fos.close();

            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            Log.i("AddPictureResult", resultUri.getPath());

            Image image = new Image(diaryEntry, resultUri.getPath(), TimeHelper.getTimeWhenImageTaken(resultUri.getPath()));
            DatabaseManager.getInstance(activity).createImage(image);
        }

        // Remove the first element since it is already processed
        imagesToCropList.remove(0);

        // If we still have images to crop, start cropping activity
        if (imagesToCropList.size() > 0) {
            startCroppingActivity(activity);
        }
    }

    /**
     * Starts the UCrop cropping activity.
     *
     * @param activity The context activity.
     */
    private static void startCroppingActivity(final Activity activity) {
        UCrop.Options options = new UCrop.Options();
        options.setStatusBarColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
        options.setToolbarColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        options.setActiveWidgetColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        UCrop.of(Uri.fromFile(new File(imagesToCropList.get(0))), Uri.fromFile(ImageManipulator.getAvailableCompressedOutputFile()))
                .withAspectRatio(1, 1)
                .withMaxResultSize(1080, 1080)
                .withOptions(options)
                .start(activity);
    }
}
