package at.fhhagenberg.photo_diary.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * A class offering static methods to access all settings stored in shared preferences
 */
public class DiarySettings {
    private static final String ONBOARDING_COMPLETED = "onboardingCompleted";
    private static final String PERSONALIZATION_EMAIL = "personalizationEmail";
    private static final String PERSONALIZATION_PASSWORD = "personalizationPassword";
    private static final String PERSONALIZATION_FIRST_NAME = "personalizationFirstName";
    private static final String PERSONALIZATION_LAST_NAME = "personalizationLastName";
    private static final String IS_LOGGED_IN = "isLoggedIn";
    private static final String HIGH_RESOLUTION_SAVING = "highResolutionSaving";
    private static final String IS_NOTIFICATION_ENABLED = "isNotificationEnabled";
    private static final String PROFILE_PICTURE_PATH = "profilePicturePath";
    private static final String COMPLETED_DIARY_AUTOPLAY = "completedDiaryAutoplay";
    private static final String COMPLETED_DIARY_GIF_SPEED = "completedDiaryGifSpeed";
    private static final String COMPLETED_DIARY_EXPORT_QUALITY = "completedDiaryExportQuality";

    private static SharedPreferences getSharedPreferences(final Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static boolean isOnboardingComplete(final Context context) {
        return getSharedPreferences(context).getBoolean(ONBOARDING_COMPLETED, false);
    }

    public static void setOnboardingCompleted(final Context context, boolean value) {
        getSharedPreferences(context).edit().putBoolean(ONBOARDING_COMPLETED, value).apply();
    }

    public static void setPersonalizationFirstName(final Context context, String firstName) {
        getSharedPreferences(context).edit().putString(PERSONALIZATION_FIRST_NAME, firstName).apply();
    }

    public static String getPersonalizationFirstName(final Context context) {
        return getSharedPreferences(context).getString(PERSONALIZATION_FIRST_NAME, "");
    }

    public static String getPersonalizationEmail(final Context context) {
        return getSharedPreferences(context).getString(PERSONALIZATION_EMAIL, "");
    }

    public static void setPersonalizationPassword(final Context context, String password) {
        getSharedPreferences(context).edit().putString(PERSONALIZATION_PASSWORD, password).apply();
    }

    public static String getPersonalizationPassword(final Context context) {
        return getSharedPreferences(context).getString(PERSONALIZATION_PASSWORD, "");
    }

    public static void setPersonalizationEmail(final Context context, String email) {
        getSharedPreferences(context).edit().putString(PERSONALIZATION_EMAIL, email).apply();
    }

    public static void setPersonalizationLastName(final Context context, String lastName) {
        getSharedPreferences(context).edit().putString(PERSONALIZATION_LAST_NAME, lastName).apply();
    }

    public static String getPersonalizationLastName(final Context context) {
        return getSharedPreferences(context).getString(PERSONALIZATION_LAST_NAME, "");
    }

    public static void setIsLoggedIn(final Context context, boolean isLoggedIn) {
        getSharedPreferences(context).edit().putBoolean(IS_LOGGED_IN, isLoggedIn).apply();
    }

    public static boolean isLoggedIn(final Context context) {
        return getSharedPreferences(context).getBoolean(IS_LOGGED_IN, false);
    }

    public static void setHighResolutionSavingEnabled(final Context context, boolean value) {
        getSharedPreferences(context).edit().putBoolean(HIGH_RESOLUTION_SAVING, value).apply();
    }

    public static boolean isHighResolutionSavingEnabled(final Context context) {
        return getSharedPreferences(context).getBoolean(HIGH_RESOLUTION_SAVING, false);
    }

    public static void setNotificationEnabled(final Context context, boolean value) {
        getSharedPreferences(context).edit().putBoolean(IS_NOTIFICATION_ENABLED, value).apply();
    }

    public static boolean isNotificationEnabled(final Context context) {
        return getSharedPreferences(context).getBoolean(IS_NOTIFICATION_ENABLED, true);
    }

    // TODO: Profile picture should be placed in User-Model later on
    public static void setProfilePicturePath(final Context context, String path) {
        getSharedPreferences(context).edit().putString(PROFILE_PICTURE_PATH, path).apply();
    }

    public static String getProfilePicturePath(final Context context) {
        return getSharedPreferences(context).getString(PROFILE_PICTURE_PATH, "");
    }

    public static void setCompletedDiaryAutoplay(final Context context, int duration) {
        getSharedPreferences(context).edit().putInt(COMPLETED_DIARY_AUTOPLAY, duration).apply();
    }

    public static int getCompletedDiaryAutoplay(final Context context) {
        return getSharedPreferences(context).getInt(COMPLETED_DIARY_AUTOPLAY, 3000);
    }

    public static void setCompletedDiaryGifSpeed(final Context context, int duration) {
        getSharedPreferences(context).edit().putInt(COMPLETED_DIARY_GIF_SPEED, duration).apply();
    }

    public static int getCompletedDiaryGifSpeed(final Context context) {
        return getSharedPreferences(context).getInt(COMPLETED_DIARY_GIF_SPEED, 3500);
    }

    public static void setCompletedDiaryExportQuality(final Context context, int quality) {
        getSharedPreferences(context).edit().putInt(COMPLETED_DIARY_EXPORT_QUALITY, quality).apply();
    }

    public static int getCompletedDiaryExportQuality(final Context context) {
        return getSharedPreferences(context).getInt(COMPLETED_DIARY_EXPORT_QUALITY, 1080);
    }
}
