package at.fhhagenberg.photo_diary.helper;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import at.fhhagenberg.photo_diary.model.Image;

/**
 * Class that handles encoding of Images to gif files.
 */
public class GifEncoder {

    /**
     * Encodes a array of images with the given settings as gif file.
     *
     * @param images  The Image array that has to be encoded to gif.
     * @param name    The name of the diary entry the images belong to.
     * @param delay   The delay between each picture in the resulting gif.
     * @param quality The resolution of the resulting gif in pixels. Because the result gif
     *                is always in square form, this value determines both edges. If the images
     *                in the array are not of the correct size, they are scaled to the given size.
     * @return Returns always null.
     */
    public static File encode(Image[] images, String name, int delay, int quality) {

        if (quality > 1080) {
            quality = 1080;
        }

        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/Viary/Exported");

        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }


        Calendar c = Calendar.getInstance();
        File file = new File(dir, name + "_at_" + c.get(Calendar.DATE) + c.get(Calendar.HOUR) + c.get(Calendar.MINUTE) + c.get(Calendar.SECOND) + ".gif");
        Log.i("CompletedDiary", file.getAbsolutePath());

        //TODO: Replace with better saving function
        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(file);
            outStream.write(generateGIF(imagesToBitmap(images, quality), delay, quality));
            outStream.close();
            Log.i("CompletedDiary", "Finished writing out");
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Generates a gif from an ArrayList of Bitmaps and returns it as byte[] that can be
     * written to a *.gif file using FileOutputStream
     *
     * @param bitmaps A arraylist of bitmaps to be converted to a gif
     * @return The encoded gif as byte[]
     */
    private static byte[] generateGIF(ArrayList<Bitmap> bitmaps, int delay, int quality) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        AnimatedGifEncoder encoder = new AnimatedGifEncoder();
        encoder.setDelay(delay);
        encoder.setQuality(20);
        encoder.setSize(quality, quality);
        encoder.start(bos);
        for (Bitmap bitmap : bitmaps) {
            encoder.addFrame(bitmap);
        }
        encoder.finish();
        return bos.toByteArray();
    }

    /**
     * Converts an array of Image objects to an ArrayList of Bitmaps
     *
     * @param images The array of images that has to be converted
     * @return An ArrayList of bitmaps created from the given Images
     */
    private static ArrayList<Bitmap> imagesToBitmap(Image[] images, int quality) {
        ArrayList<Bitmap> bitmaps = new ArrayList<>();

        //TODO: Check what happens when getThumbnailFromFile returns null (maybe because image doesn't exist anymore?)
        for (Image image : images) {
            Bitmap bmp = ImageManipulator.getThumbnailFromFile(image.getFilePath(), quality, quality);
            if (bmp.getHeight() != quality || bmp.getWidth() != quality) {
                Log.i("GifEncoder", "Create scaled bitmap");
                bmp = Bitmap.createScaledBitmap(bmp, quality, quality, true);
            }
            bitmaps.add(bmp);
        }

        return bitmaps;
    }
}
