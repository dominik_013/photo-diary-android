package at.fhhagenberg.photo_diary.helper;


import android.content.Context;
import android.media.ExifInterface;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;

import at.fhhagenberg.photo_diary.R;

/**
 * Class that provides methods to create a time stamp for saving an image.
 */
public class TimeHelper {

    /**
     * Parses a date-time to a readable String representation
     *
     * @param context  Context
     * @param dateTime The time which has to be parsed
     * @return A string representation of the current date in the format "dd.MM.yyyy"
     */
    public static String dateTimeToReadableDate(Context context, DateTime dateTime) {
        if (dateTime == null) return "No time specified";
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(context.getResources().getString(R.string.date_format_pattern));
        return dateTime.toString(dateTimeFormatter);
    }

    /**
     * If the image has the necessary exif-information we return the time when the image was taken
     *
     * @param imagePath The path to the image
     * @return The date-time when the image was taken
     */
    public static DateTime getTimeWhenImageTaken(String imagePath) {
        DateTime creationTime = null;

        try {
            ExifInterface exifInterface = new ExifInterface(imagePath);
            String dateString = exifInterface.getAttribute(ExifInterface.TAG_DATETIME);
            DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("YYYY:MM:dd HH:mm:ss");
            if (dateString != null) {
                creationTime = dateTimeFormatter.parseDateTime(dateString);
            }
        } catch (IOException ex) {
            Log.e("DiaryEntryDetail", ex.getMessage());
        }

        return creationTime;
    }
}
