package at.fhhagenberg.photo_diary.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import at.fhhagenberg.photo_diary.ui.navigation.NavigationCurrentActiveEntriesFragment;
import at.fhhagenberg.photo_diary.ui.navigation.NavigationFeedFragment;
import at.fhhagenberg.photo_diary.ui.navigation.NavigationFragment;
import at.fhhagenberg.photo_diary.ui.navigation.NavigationProfileFragment;

public class NavigationPagerAdapter extends FragmentStatePagerAdapter {

    private int count;
    private SparseArray<NavigationFragment> registeredFragments = new SparseArray<>();

    public NavigationPagerAdapter(FragmentManager fragmentManager, int count) {
        super(fragmentManager);
        this.count = count;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment returnFragment = null;
        switch (position) {
            case 0:
                returnFragment = new NavigationFeedFragment();
                break;
            case 1:
                returnFragment = new NavigationCurrentActiveEntriesFragment();
                break;
            case 2:
                returnFragment = new NavigationProfileFragment();
                break;
            default:
                break;
        }

        return returnFragment;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        NavigationFragment fragment =
                (NavigationFragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public NavigationFragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}