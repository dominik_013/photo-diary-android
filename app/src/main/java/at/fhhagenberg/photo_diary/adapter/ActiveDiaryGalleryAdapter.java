package at.fhhagenberg.photo_diary.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.helper.ImageManipulator;
import at.fhhagenberg.photo_diary.interfaces.GalleryEntryClickListener;
import at.fhhagenberg.photo_diary.model.Image;
import at.fhhagenberg.photo_diary.ui.GalleryDetailActivity;
import at.fhhagenberg.photo_diary.viewholder.ActiveDiaryGalleryViewHolder;

/**
 * A class that handles the gallery entries in the ActiveDiaryEntry gallery.
 */
public class ActiveDiaryGalleryAdapter extends RecyclerView.Adapter<ActiveDiaryGalleryViewHolder> implements GalleryEntryClickListener {

    private Image[] images;
    private Context context;
    private int entryID;

    /**
     * Creates a new ActiveDiaryGalleryAdapter
     *
     * @param images  The images that are shown in the gallery
     * @param context The context in which this adapter is used
     */
    public ActiveDiaryGalleryAdapter(Image[] images, Context context, int entryID) {
        this.images = images;
        this.context = context;
        this.entryID = entryID;
    }

    @Override
    public ActiveDiaryGalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.active_diary_gallery_item, parent, false);
        return new ActiveDiaryGalleryViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(ActiveDiaryGalleryViewHolder holder, int position) {
        holder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        holder.imageView.setImageBitmap(ImageManipulator.getThumbnailFromFile(images[position].getFilePath(), 256, 256));
    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    @Override
    public void onGalleryItemClick(int position) {
        Log.i("ActiveDiaryGallery", "Clicked item " + position);
        Intent i = new Intent(context, GalleryDetailActivity.class);
        i.putExtra(GalleryDetailActivity.EXTRA_GALLERY_POSITION, position);
        i.putExtra(GalleryDetailActivity.EXTRA_ENTRY_ID, entryID);
        context.startActivity(i);
    }
}
