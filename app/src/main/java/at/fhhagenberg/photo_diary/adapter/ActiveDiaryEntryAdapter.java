package at.fhhagenberg.photo_diary.adapter;


import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.helper.ImageManipulator;
import at.fhhagenberg.photo_diary.helper.TimeHelper;
import at.fhhagenberg.photo_diary.interfaces.DiaryEntryClickListener;
import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.model.Image;
import at.fhhagenberg.photo_diary.ui.DiaryEntryDetailActivity;
import at.fhhagenberg.photo_diary.viewholder.ActiveDiaryEntryViewHolder;

public class ActiveDiaryEntryAdapter extends RecyclerView.Adapter<ActiveDiaryEntryViewHolder> implements DiaryEntryClickListener {

    public static final String EXTRA_SELECTED_DIARY_ENTRY = "selectedDiaryEntry";
    public static final int REQUEST_ACTIVITY_DIARY_ENTRY_DETAIL = 0;

    private final Activity activity;
    private final List<DiaryEntry> diaryEntryList;
    private AddImageFromCardClickListener addImageFromCardClickListener;

    /**
     * Creates a ActiveDiaryEntryAdapter
     *
     * @param activity                      The calling activity
     * @param addImageFromCardClickListener The listener which will react to onAddFromGalleryClick
     */
    public ActiveDiaryEntryAdapter(final Activity activity, AddImageFromCardClickListener addImageFromCardClickListener) {
        this.activity = activity;
        this.diaryEntryList = new ArrayList<>();
        this.addImageFromCardClickListener = addImageFromCardClickListener;
    }

    @Override
    public ActiveDiaryEntryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.active_diary_entry_item, parent, false);

        return new ActiveDiaryEntryViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(ActiveDiaryEntryViewHolder holder, int position) {
        DiaryEntry entry = diaryEntryList.get(position);

        if (entry.getImages().size() != 0) {
            int idx = (int) (Math.random() * entry.getImages().size());
            Image img = (Image) entry.getImages().toArray()[idx];
            holder.imageViewThumbnail.setImageBitmap(ImageManipulator.getThumbnailFromFile(img.getFilePath(), 256, 256));
        } else {
            holder.imageViewThumbnail.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), R.drawable.default_entry_image));
        }

        holder.textViewName.setText(entry.getName());
        DateTime fromTime = entry.getFrom();
        DateTime toTime = entry.getTo();
        if (fromTime == null || toTime == null) {
            holder.textViewFrom.setText(activity.getResources().getString(R.string.text_diary_entry_item_from_to_no_time));
            holder.textViewTo.setVisibility(View.GONE);
            holder.textViewFromToConnection.setVisibility(View.GONE);
        } else {
            holder.textViewFrom.setText(TimeHelper.dateTimeToReadableDate(activity, fromTime));
            holder.textViewTo.setText(TimeHelper.dateTimeToReadableDate(activity, toTime));
        }
        int count = entry.getImages().size();
        holder.textViewNoOfPictures.setText(activity.getResources().getQuantityString(R.plurals.text_diary_entry_item_picture_count, count, count));
    }

    @Override
    public int getItemCount() {
        return diaryEntryList.size();
    }

    @Override
    public void onDiaryEntryClick(int position) {
        Intent intent = new Intent(activity, DiaryEntryDetailActivity.class);
        intent.putExtra(EXTRA_SELECTED_DIARY_ENTRY, diaryEntryList.get(position).getExternalId());
        activity.startActivityForResult(intent, REQUEST_ACTIVITY_DIARY_ENTRY_DETAIL);
    }

    /**
     * Handles the onCameraClick icon for each diary-entry
     *
     * @param position The position from which diary entry we clicked the camera icon
     */
    @Override
    public void onAddFromCameraClick(int position) {
        addImageFromCardClickListener.onAddFromCameraClick(diaryEntryList.get(position));
    }

    /**
     * Handles the onGalleryClick icon for each diary-entry
     *
     * @param position The position from which diary entry we clicked the gallery icon
     */
    @Override
    public void onAddFromGalleryClick(int position) {
        addImageFromCardClickListener.onAddFromGalleryClick(diaryEntryList.get(position));
    }

    /**
     * Clears the current list and updates it with the passed collection
     *
     * @param articleList The new list which will be displayed
     */
    public void updataListData(Collection<DiaryEntry> articleList) {
        this.diaryEntryList.clear();
        this.diaryEntryList.addAll(articleList);
        notifyDataSetChanged();
    }

    /**
     * Interface to forward onAddFromGalleryClick to the calling activity/fragment
     */
    public interface AddImageFromCardClickListener {
        /**
         * If we want to add images from the gallery
         *
         * @param diaryEntry The diary-entry of the selected card
         */
        void onAddFromGalleryClick(DiaryEntry diaryEntry);

        /**
         * If we want to add images from the camera
         *
         * @param diaryEntry The diary-entry of the selected card
         */
        void onAddFromCameraClick(DiaryEntry diaryEntry);
    }
}