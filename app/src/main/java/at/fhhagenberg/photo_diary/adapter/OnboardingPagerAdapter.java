package at.fhhagenberg.photo_diary.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import at.fhhagenberg.photo_diary.ui.onboarding.OnboardingFragment;
import at.fhhagenberg.photo_diary.ui.onboarding.OnboardingPersonalizationFragment;
import at.fhhagenberg.photo_diary.ui.onboarding.OnboardingSaveMemoriesFragment;
import at.fhhagenberg.photo_diary.ui.onboarding.OnboardingShareStoriesFragment;
import at.fhhagenberg.photo_diary.ui.onboarding.OnboardingWelcomeFragment;

public class OnboardingPagerAdapter extends FragmentStatePagerAdapter {
    private int count;
    private SparseArray<OnboardingFragment> registeredFragments = new SparseArray<>();

    public OnboardingPagerAdapter(FragmentManager fragmentManager, int count) {
        super(fragmentManager);
        this.count = count;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment returnFragment;
        switch (position) {
            case 0:
                returnFragment = new OnboardingWelcomeFragment();
                break;

            case 1:
                returnFragment = new OnboardingSaveMemoriesFragment();
                break;

            case 2:
                returnFragment = new OnboardingShareStoriesFragment();
                break;

            case 3:
                returnFragment = new OnboardingPersonalizationFragment();
                break;

            default:
                returnFragment = null;
                break;
        }

        return returnFragment;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        OnboardingFragment fragment =
                (OnboardingFragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public OnboardingFragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}
