package at.fhhagenberg.photo_diary.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.helper.ImageManipulator;
import at.fhhagenberg.photo_diary.interfaces.DiaryEntryClickListener;
import at.fhhagenberg.photo_diary.model.DiaryEntry;
import at.fhhagenberg.photo_diary.model.Image;
import at.fhhagenberg.photo_diary.ui.CompletedDiaryEntryDetailActivity;
import at.fhhagenberg.photo_diary.viewholder.CompletedDiaryEntryViewHolder;

import static com.facebook.FacebookSdk.getApplicationContext;


public class CompletedDiaryEntryAdapter extends RecyclerView.Adapter<CompletedDiaryEntryViewHolder> implements DiaryEntryClickListener {

    private final Activity activity;
    private final List<DiaryEntry> diaryEntryList;

    public static final String EXTRA_SELECTED_DIARY_ENTRY = "selectedDiaryEntry";

    /**
     * Creates a new CompletedDiaryEntryAdapter
     *
     * @param activity The activitythis adapter belongs to
     */
    public CompletedDiaryEntryAdapter(final Activity activity) {
        this.activity = activity;
        diaryEntryList = new ArrayList<>();
    }

    @Override
    public CompletedDiaryEntryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.completed_diary_entry_item, parent, false);

        Log.i("CompletedAdapter", "Called onCreateViewHolder");

        return new CompletedDiaryEntryViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(CompletedDiaryEntryViewHolder holder, int position) {
        DiaryEntry diaryEntry = diaryEntryList.get(position);

        holder.textViewName.setText(diaryEntry.getName());
        int count = diaryEntry.getImages().size();
        holder.textViewNumberOfPictures.setText(activity.getResources().getQuantityString(R.plurals.text_diary_entry_item_picture_count, count, count));

        if (diaryEntry.getImages().size() != 0) {
            int idx = (int) (Math.random() * diaryEntry.getImages().size());
            Image img = (Image) diaryEntry.getImages().toArray()[idx];
            holder.imageViewPreview.setImageBitmap(ImageManipulator.getThumbnailFromFile(img.getFilePath(), 256, 256));
        } else {
            holder.imageViewPreview.setImageBitmap(BitmapFactory.decodeResource(activity.getResources(), R.drawable.default_entry_image));
        }
    }

    @Override
    public int getItemCount() {
        return diaryEntryList.size();
    }

    /**
     * This method updates the list with the new entries
     *
     * @param diaryEntryList The new entries which will be displayed in the recycler view
     */
    public void updateListData(final List<DiaryEntry> diaryEntryList) {
        this.diaryEntryList.clear();

        this.diaryEntryList.addAll(diaryEntryList);
        notifyDataSetChanged();
    }

    @Override
    public void onDiaryEntryClick(int position) {

        DiaryEntry entry = diaryEntryList.get(position);
        if (entry.getImages().size() < 1) {
            Toast.makeText(getApplicationContext(), "There are no images in this entry", Toast.LENGTH_SHORT).show();
        } else {
            Intent i = new Intent(activity, CompletedDiaryEntryDetailActivity.class);
            i.putExtra(EXTRA_SELECTED_DIARY_ENTRY, diaryEntryList.get(position).getExternalId());
            activity.startActivity(i);
        }

    }

    // Do nothing here..
    @Override
    public void onAddFromCameraClick(int position) {
    }

    @Override
    public void onAddFromGalleryClick(int position) {

    }
}
