package at.fhhagenberg.photo_diary.client;

import at.fhhagenberg.photo_diary.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {
    /**
     * Invokes the method "register" on the rest-service
     *
     * @param user The User object we want to Pass
     * @return A Call Object for the User we created
     */
    @POST("register")
    Call<User> registerUser(@Body User user);
}
