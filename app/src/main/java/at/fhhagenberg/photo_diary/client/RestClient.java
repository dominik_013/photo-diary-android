package at.fhhagenberg.photo_diary.client;

import android.text.TextUtils;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static final String BASE_URL = "http://192.168.8.113:8080/api/";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static Retrofit retrofit;
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    /**
     * Creates a service without authentication
     *
     * @param serviceClass The service-interface which will be used to handle all requests
     * @param <S>          The service-interface type
     * @return A service-interface object which can be used to process requests
     */
    public static <S> S createService(Class<S> serviceClass) {
        return createService(serviceClass, null, null);
    }

    /**
     * Creates a service with the passed authentication details for username/password
     *
     * @param serviceClass The service-interface which will be used to handle all requests
     * @param username     The username used to authenticate the user
     * @param password     The password used to authenticate the user
     * @param <S>          The service-interface type
     * @return A service-interface object which can be used to process requests
     */
    public static <S> S createService(Class<S> serviceClass, String username, String password) {
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            String authToken = Credentials.basic(username, password);
            return createService(serviceClass, authToken);
        }

        return createService(serviceClass, null);
    }

    /**
     * Creates a service with the passed authentication token
     *
     * @param serviceClass The service-interface which will be used to handle all requests
     * @param authToken    The token used for basic authentication (generated from username + password)
     * @param <S>          The service-interface type
     * @return A service-interface object which can be used to process requests
     */
    public static <S> S createService(Class<S> serviceClass, final String authToken) {
        // Append auth-token if available
        if (!TextUtils.isEmpty(authToken)) {
            BasicAuthenticationInterceptor interceptor = new BasicAuthenticationInterceptor(authToken);
            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);
                retrofit = builder.client(httpClient.build()).build();
            }
        }

        if (retrofit == null) {
            retrofit = builder.build();
        }

        return retrofit.create(serviceClass);
    }
}
