package at.fhhagenberg.photo_diary.client;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


/**
 * An interceptor which will append Authorization information to every web-request
 * we make.
 * The interceptor can be created with an AuthToken or with a Username/Password combination
 */
public class BasicAuthenticationInterceptor implements Interceptor {

    private String credentials;

    /**
     * Creates an interceptor with the provided authentication token
     *
     * @param authToken The token which was generated through username + password
     */
    public BasicAuthenticationInterceptor(String authToken) {
        this.credentials = authToken;
    }

    /**
     * Creates an interceptor with the provided username + password
     *
     * @param user     The username which will be used for authentication
     * @param password The password which will be used for authentication
     */
    public BasicAuthenticationInterceptor(String user, String password) {
        this.credentials = Credentials.basic(user, password);
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request authenticatedRequest = request.newBuilder()
                .header("Authorization", credentials)
                .build();
        return chain.proceed(authenticatedRequest);
    }
}
