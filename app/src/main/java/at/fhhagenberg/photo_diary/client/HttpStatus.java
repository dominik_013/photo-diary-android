package at.fhhagenberg.photo_diary.client;

/**
 * Represents our used status codes coming from the
 * web service
 */
public class HttpStatus {
    public final static int OK = 200;
    public final static int INTERNAL_SERVER_ERROR = 500;
}
