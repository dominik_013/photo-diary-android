package at.fhhagenberg.photo_diary.interfaces;

/**
 * Interface to handle clicks on gallery items
 */
public interface GalleryEntryClickListener {

    /**
     * Gets called when a image in the gallery overview is clicked on
     *
     * @param position The position of the entry in the gallery
     */
    void onGalleryItemClick(int position);
}
