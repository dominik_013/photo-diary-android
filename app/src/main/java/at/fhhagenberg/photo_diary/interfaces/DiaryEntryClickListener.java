package at.fhhagenberg.photo_diary.interfaces;

/**
 * Interface for the diary-entry-item View holder
 */
public interface DiaryEntryClickListener {

    /**
     * Gets called when the diary entry is clicked.
     *
     * @param position The position of the entry in the list
     */
    void onDiaryEntryClick(int position);

    /**
     * Gets called when the camera icon in the diary entry is clicked.
     *
     * @param position The position of the entry in the list.
     */
    void onAddFromCameraClick(int position);

    /**
     * Gets called when the gallery icon in the diary entry is clicked.
     *
     * @param position The position of the entry in the list.
     */
    void onAddFromGalleryClick(int position);
}
