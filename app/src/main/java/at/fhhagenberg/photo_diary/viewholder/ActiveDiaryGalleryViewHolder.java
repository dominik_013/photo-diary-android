package at.fhhagenberg.photo_diary.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.interfaces.GalleryEntryClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ActiveDiaryGalleryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.active_diary_gallery_item)
    public ImageView imageView;

    private GalleryEntryClickListener listener;

    public ActiveDiaryGalleryViewHolder(View itemView, GalleryEntryClickListener listener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.listener = listener;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        listener.onGalleryItemClick(getAdapterPosition());
    }
}
