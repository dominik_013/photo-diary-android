package at.fhhagenberg.photo_diary.viewholder;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.interfaces.DiaryEntryClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActiveDiaryEntryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.image_diary_entry_item_thumbnail)
    public ImageView imageViewThumbnail;

    @BindView(R.id.txt_diary_entry_name)
    public TextView textViewName;

    @BindView(R.id.txt_diary_entry_from)
    public TextView textViewFrom;

    @BindView(R.id.txt_diary_entry_to)
    public TextView textViewTo;

    @BindView(R.id.txt_diary_entry_from_to_connection)
    public TextView textViewFromToConnection;

    @BindView(R.id.txt_diary_entry_number_of_pictures)
    public TextView textViewNoOfPictures;

    private final DiaryEntryClickListener listener;

    public ActiveDiaryEntryViewHolder(View itemView, DiaryEntryClickListener listener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.listener = listener;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        listener.onDiaryEntryClick(getAdapterPosition());
    }

    @OnClick(R.id.btn_diary_entry_item_add_camera)
    void onAddFromCameraClick() {
        listener.onAddFromCameraClick(getAdapterPosition());
    }

    @OnClick(R.id.btn_diary_entry_item_add_gallery)
    void onAddFromGalleryClick() {
        listener.onAddFromGalleryClick(getAdapterPosition());
    }
}
