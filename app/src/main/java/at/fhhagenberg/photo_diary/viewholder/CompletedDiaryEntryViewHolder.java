package at.fhhagenberg.photo_diary.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import at.fhhagenberg.photo_diary.R;
import at.fhhagenberg.photo_diary.interfaces.DiaryEntryClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CompletedDiaryEntryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.txt_completed_entry_name)
    public TextView textViewName;

    @BindView(R.id.txt_completed_entry_number_of_pictures)
    public TextView textViewNumberOfPictures;

    @BindView(R.id.image_completed_entry_preview)
    public ImageView imageViewPreview;

    private DiaryEntryClickListener listener;

    public CompletedDiaryEntryViewHolder(View itemView, DiaryEntryClickListener listener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.listener = listener;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        listener.onDiaryEntryClick(getAdapterPosition());
    }
}
